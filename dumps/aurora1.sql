-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: aurora
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_preferences_dashboard_id_575b1104_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_403f60f` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{\"positions\":{},\"columns\":{},\"disabled\":{},\"collapsed\":{\"module_1\":false}}','rooms-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(19,'Can add kv store',7,'add_kvstore'),(20,'Can change kv store',7,'change_kvstore'),(21,'Can delete kv store',7,'delete_kvstore'),(22,'Can add migration history',8,'add_migrationhistory'),(23,'Can change migration history',8,'change_migrationhistory'),(24,'Can delete migration history',8,'delete_migrationhistory'),(25,'Can add bookmark',9,'add_bookmark'),(26,'Can change bookmark',9,'change_bookmark'),(27,'Can delete bookmark',9,'delete_bookmark'),(28,'Can add dashboard preferences',10,'add_dashboardpreferences'),(29,'Can change dashboard preferences',10,'change_dashboardpreferences'),(30,'Can delete dashboard preferences',10,'delete_dashboardpreferences'),(31,'Can add menu_item',11,'add_sitemenu'),(32,'Can change menu_item',11,'change_sitemenu'),(33,'Can delete menu_item',11,'delete_sitemenu'),(34,'Can add site_setting',12,'add_settings'),(35,'Can change site_setting',12,'change_settings'),(36,'Can delete site_setting',12,'delete_settings'),(37,'Can add Текстовый блок',13,'add_textblock'),(38,'Can change Текстовый блок',13,'change_textblock'),(39,'Can delete Текстовый блок',13,'delete_textblock'),(40,'Can add page_item',14,'add_page'),(41,'Can change page_item',14,'change_page'),(42,'Can delete page_item',14,'delete_page'),(43,'Can add файл',15,'add_pagedoc'),(44,'Can change файл',15,'change_pagedoc'),(45,'Can delete файл',15,'delete_pagedoc'),(46,'Can add картинка',16,'add_pagepic'),(47,'Can change картинка',16,'change_pagepic'),(48,'Can delete картинка',16,'delete_pagepic'),(49,'Can add meta',17,'add_metadata'),(50,'Can change meta',17,'change_metadata'),(51,'Can delete meta',17,'delete_metadata'),(52,'Can add news_category',18,'add_newscategory'),(53,'Can change news_category',18,'change_newscategory'),(54,'Can delete news_category',18,'delete_newscategory'),(55,'Can add news_item',19,'add_news'),(56,'Can change news_item',19,'change_news'),(57,'Can delete news_item',19,'delete_news'),(64,'Can add конфигурация комнат',22,'add_roomconfig'),(65,'Can change конфигурация комнат',22,'change_roomconfig'),(66,'Can delete конфигурация комнат',22,'delete_roomconfig'),(67,'Can add отель',23,'add_hotel'),(68,'Can change отель',23,'change_hotel'),(69,'Can delete отель',23,'delete_hotel'),(70,'Can add тип комнаты',24,'add_roomtype'),(71,'Can change тип комнаты',24,'change_roomtype'),(72,'Can delete тип комнаты',24,'delete_roomtype'),(73,'Can add комната',25,'add_room'),(74,'Can change комната',25,'change_room'),(75,'Can delete комната',25,'delete_room'),(76,'Can add тип цены',26,'add_pricetype'),(77,'Can change тип цены',26,'change_pricetype'),(78,'Can delete тип цены',26,'delete_pricetype'),(79,'Can add сезон',27,'add_season'),(80,'Can change сезон',27,'change_season'),(81,'Can delete сезон',27,'delete_season'),(82,'Can add цена',28,'add_price'),(83,'Can change цена',28,'change_price'),(84,'Can delete цена',28,'delete_price'),(85,'Can add тип отеля',29,'add_hoteltype'),(86,'Can change тип отеля',29,'change_hoteltype'),(87,'Can delete тип отеля',29,'delete_hoteltype'),(88,'Can add фотография',30,'add_roomphoto'),(89,'Can change фотография',30,'change_roomphoto'),(90,'Can delete фотография',30,'delete_roomphoto'),(91,'Can add панорама',31,'add_roompanorama'),(92,'Can change панорама',31,'change_roompanorama'),(93,'Can delete панорама',31,'delete_roompanorama');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'grigl','','','grigl@mail.ru','pbkdf2_sha256$10000$5BwfH7IzPStm$NKjRxOeuCbbzEA0+DDreq6satbA/8bDnHeyJVKge9Hc=',1,1,1,'2013-05-29 13:44:36','2013-05-29 12:59:33');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-05-29 14:08:19',1,22,'1','Гостиница «ОДИССЕЙ»',1,''),(2,'2013-05-29 14:09:20',1,22,'2','Гостиницы «РИФ» и «Шелл»',1,''),(3,'2013-05-29 14:10:37',1,22,'3','Таунхаусы «Мыс», «Лагуна», «Бриз»',1,''),(4,'2013-05-29 14:10:56',1,22,'1','Гостиница «Одиссей»',2,'Изменен title.'),(5,'2013-05-29 14:14:25',1,22,'4','Таунхаусы «Нептун», «Посейдон»',1,''),(6,'2013-05-29 14:48:53',1,26,'1','Будние дни',1,''),(7,'2013-05-29 14:50:05',1,27,'1','Проживание зима-весна',1,''),(8,'2013-05-29 14:52:30',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"999\".'),(9,'2013-05-29 14:53:04',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Удален цена \"1000.00\".'),(10,'2013-05-29 14:56:05',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"2000\".'),(11,'2013-05-29 14:57:21',1,26,'2','Выходные дни сутки',1,''),(12,'2013-05-29 15:02:22',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price_type и price для цена \"3600\".'),(13,'2013-05-29 15:04:23',1,26,'3','Выходные дни 3 дня / 2 ночи пятница-воскресенье',1,''),(14,'2013-05-29 15:07:17',1,26,'4','Будние дни (3 дня / 2 ночи воскресенье-пятница)',1,''),(15,'2013-05-29 15:07:36',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"3600\". Добавлен цена \"5400\". Добавлен цена \"3000\". Изменены price_type и price для цена \"2000\".'),(16,'2013-05-29 15:08:35',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"3000\". Добавлен цена \"6000\". Добавлен цена \"9000\". Добавлен цена \"45000\".'),(17,'2013-05-29 15:08:39',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"4500.00\".'),(18,'2013-05-29 15:13:08',1,24,'3','Studio из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"2600\". Добавлен цена \"5000\". Добавлен цена \"7500\". Добавлен цена \"3900\".'),(19,'2013-05-29 15:15:04',1,24,'4','Penthouse из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"5000\". Добавлен цена \"10000\". Добавлен цена \"15000\". Добавлен цена \"7500\".'),(20,'2013-05-29 15:16:57',1,24,'5','2-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"1500\". Добавлен цена \"2600\". Добавлен цена \"3900\". Добавлен цена \"2250\".'),(21,'2013-05-29 15:19:04',1,24,'6','3-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"2000\". Добавлен цена \"3600\". Добавлен цена \"5400\". Добавлен цена \"3000\".'),(22,'2013-05-29 15:21:18',1,24,'7','4-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"2700\". Добавлен цена \"4800\". Добавлен цена \"7200\". Добавлен цена \"4050\".'),(23,'2013-05-29 15:23:25',1,24,'8','4-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"4000\". Добавлен цена \"7500\". Добавлен цена \"11000\". Добавлен цена \"6000\".'),(24,'2013-05-29 15:24:33',1,24,'9','5-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"4500\". Добавлен цена \"8500\". Добавлен цена \"12500\". Добавлен цена \"6750\".'),(25,'2013-05-29 15:25:28',1,24,'10','6-местный номер из конфигурации Таунхаусы «Нептун», «Посейдон»',2,'Добавлен цена \"7000\". Добавлен цена \"14000\". Добавлен цена \"21000\". Добавлен цена \"10500\".'),(26,'2013-05-29 15:33:47',1,23,'1','Одиссей',1,''),(27,'2013-05-29 15:34:59',1,23,'2','Риф',1,''),(28,'2013-05-29 15:36:02',1,23,'3','Шелл',1,''),(29,'2013-05-29 15:36:17',1,23,'2','Риф',2,'Изменен image.'),(30,'2013-05-29 15:36:27',1,23,'1','Одиссей',2,'Изменен image.'),(31,'2013-05-29 15:51:35',1,29,'1','Гостиница',1,''),(32,'2013-05-29 15:52:40',1,29,'2','Танхаус',1,''),(33,'2013-05-29 15:53:02',1,29,'3','Таунхаус',1,''),(34,'2013-05-29 15:53:17',1,29,'2','Таунхаус',2,'Изменен title.'),(35,'2013-05-29 16:00:14',1,23,'4','Мыс',1,''),(36,'2013-05-29 16:01:01',1,23,'5','Лагуна ',1,''),(37,'2013-05-29 16:01:54',1,23,'6','Бриз',1,''),(38,'2013-05-29 16:07:55',1,23,'7','Посейдон',1,''),(39,'2013-05-29 16:09:09',1,23,'8','Нептун',1,''),(41,'2013-05-29 17:00:08',1,25,'1','Studio',1,''),(42,'2013-05-29 17:01:21',1,25,'1','Studio',2,'Добавлен фотография \"Кухоная сторона\". Добавлен панорама \"Спальня\".'),(43,'2013-05-29 17:04:52',1,13,'1','text_about_aurora',1,''),(44,'2013-05-29 17:05:09',1,13,'1','text_about_aurora',2,'Изменен value.'),(45,'2013-05-29 17:05:13',1,13,'1','text_about_aurora',2,'Изменен value.'),(46,'2013-05-29 17:05:28',1,13,'1','text_about_aurora',2,'Изменен value.'),(47,'2013-05-29 21:55:42',1,27,'1','Проживание зима-весна',2,'Изменен special_offer_label.'),(48,'2013-05-29 22:00:51',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(49,'2013-05-29 22:01:04',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(50,'2013-05-29 22:06:43',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(51,'2013-05-29 22:06:51',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(52,'2013-05-30 00:59:00',1,27,'2','Проживание лето',1,''),(53,'2013-05-30 00:59:58',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"6666\".'),(54,'2013-05-30 01:00:38',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены season для цена \"2000.00\". Изменены season для цена \"6666.00\".'),(55,'2013-05-30 01:01:04',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены season для цена \"2000.00\". Изменены season для цена \"6666.00\".'),(56,'2013-05-30 01:22:57',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Удален цена \"6666.00\".');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'log entry','admin','logentry'),(7,'kv store','thumbnail','kvstore'),(8,'migration history','south','migrationhistory'),(9,'bookmark','menu','bookmark'),(10,'dashboard preferences','dashboard','dashboardpreferences'),(11,'menu_item','siteblocks','sitemenu'),(12,'site_setting','siteblocks','settings'),(13,'Текстовый блок','siteblocks','textblock'),(14,'page_item','pages','page'),(15,'файл','pages','pagedoc'),(16,'картинка','pages','pagepic'),(17,'meta','pages','metadata'),(18,'news_category','newsboard','newscategory'),(19,'news_item','newsboard','news'),(22,'конфигурация комнат','rooms','roomconfig'),(23,'отель','rooms','hotel'),(24,'тип комнаты','rooms','roomtype'),(25,'комната','rooms','room'),(26,'тип цены','rooms','pricetype'),(27,'сезон','rooms','season'),(28,'цена','rooms','price'),(29,'тип отеля','rooms','hoteltype'),(30,'фотография','rooms','roomphoto'),(31,'панорама','rooms','roompanorama');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('679c10daab64cb733912b8cfa8759296','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-06-12 13:44:36');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_news`
--

DROP TABLE IF EXISTS `newsboard_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `text` longtext NOT NULL,
  `on_main_page` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `newsboard_news_42dc49bc` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_news`
--

LOCK TABLES `newsboard_news` WRITE;
/*!40000 ALTER TABLE `newsboard_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_newscategory`
--

DROP TABLE IF EXISTS `newsboard_newscategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_newscategory`
--

LOCK TABLES `newsboard_newscategory` WRITE;
/*!40000 ALTER TABLE `newsboard_newscategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_newscategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_metadata`
--

DROP TABLE IF EXISTS `pages_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_metadata`
--

LOCK TABLES `pages_metadata` WRITE;
/*!40000 ALTER TABLE `pages_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_page`
--

DROP TABLE IF EXISTS `pages_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_at_menu` tinyint(1) NOT NULL,
  `is_at_footer_menu` tinyint(1) NOT NULL,
  `template` varchar(100) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `pages_page_63f17a16` (`parent_id`),
  KEY `pages_page_42b06ff6` (`lft`),
  KEY `pages_page_6eabc1a6` (`rght`),
  KEY `pages_page_102f80d8` (`tree_id`),
  KEY `pages_page_2a8f42e8` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_page`
--

LOCK TABLES `pages_page` WRITE;
/*!40000 ALTER TABLE `pages_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagedoc`
--

DROP TABLE IF EXISTS `pages_pagedoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagedoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagedoc_32d04bc7` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagedoc`
--

LOCK TABLES `pages_pagedoc` WRITE;
/*!40000 ALTER TABLE `pages_pagedoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagedoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagepic`
--

DROP TABLE IF EXISTS `pages_pagepic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagepic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagepic_32d04bc7` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagepic`
--

LOCK TABLES `pages_pagepic` WRITE;
/*!40000 ALTER TABLE `pages_pagepic` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagepic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_hotel`
--

DROP TABLE IF EXISTS `rooms_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `hotel_type_id` int(11) NOT NULL,
  `room_config_id` int(11) NOT NULL,
  `short_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_hotel_42423783` (`room_config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_hotel`
--

LOCK TABLES `rooms_hotel` WRITE;
/*!40000 ALTER TABLE `rooms_hotel` DISABLE KEYS */;
INSERT INTO `rooms_hotel` VALUES (1,'Одиссей',1,1,'Гостиница Одиссей','Гостиница Одиссей','images/hotel/hotel-2.png'),(2,'Риф',1,2,'Гостиница Риф','Гостиница Риф','images/hotel/hotel-3.png'),(3,'Шелл',1,2,'Гостиница Шелл','Гостиница Шелл','images/hotel/hotel_2.png'),(4,'Мыс',2,3,'Таунхаус Мыс','Таунхаус Мыс','images/hotel/townhouse.png'),(5,'Лагуна ',2,3,'Таунхаус Лагуна','Таунхаус Лагуна','images/hotel/townhouse_1.png'),(6,'Бриз',2,3,'Таунхаус Бриз','Таунхаус Бриз','images/hotel/townhouse_2.png'),(7,'Посейдон',3,4,'Таунхаус Посейдон','Таунхаус Посейдон','images/hotel/townhouse2.png'),(8,'Нептун',3,4,'Таунхаус Нептун','Таунхаус Нептун','images/hotel/townhouse2_1.png');
/*!40000 ALTER TABLE `rooms_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_hoteltype`
--

DROP TABLE IF EXISTS `rooms_hoteltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_hoteltype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `label` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_hoteltype`
--

LOCK TABLES `rooms_hoteltype` WRITE;
/*!40000 ALTER TABLE `rooms_hoteltype` DISABLE KEYS */;
INSERT INTO `rooms_hoteltype` VALUES (1,'Гостиница',''),(2,'Таунхаус','на 4 человек +2 доп. места'),(3,'Таунхаус','на 6 человек +2 доп. места');
/*!40000 ALTER TABLE `rooms_hoteltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_price`
--

DROP TABLE IF EXISTS `rooms_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_price_4e1f844b` (`room_type_id`),
  KEY `rooms_price_39d67f40` (`price_type_id`),
  KEY `rooms_price_23579bcd` (`season_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_price`
--

LOCK TABLES `rooms_price` WRITE;
/*!40000 ALTER TABLE `rooms_price` DISABLE KEYS */;
INSERT INTO `rooms_price` VALUES (2,1,1,1,'2000.00'),(3,1,2,1,'3600.00'),(4,1,3,1,'5400.00'),(5,1,4,1,'3000.00'),(6,2,1,1,'3000.00'),(7,2,2,1,'6000.00'),(8,2,3,1,'9000.00'),(9,2,4,1,'4500.00'),(10,3,1,1,'2600.00'),(11,3,2,1,'5000.00'),(12,3,3,1,'7500.00'),(13,3,4,1,'3900.00'),(14,4,1,1,'5000.00'),(15,4,2,1,'10000.00'),(16,4,3,1,'15000.00'),(17,4,4,1,'7500.00'),(18,5,1,1,'1500.00'),(19,5,2,1,'2600.00'),(20,5,3,1,'3900.00'),(21,5,4,1,'2250.00'),(22,6,1,1,'2000.00'),(23,6,2,1,'3600.00'),(24,6,3,1,'5400.00'),(25,6,4,1,'3000.00'),(26,7,1,1,'2700.00'),(27,7,2,1,'4800.00'),(28,7,3,1,'7200.00'),(29,7,4,1,'4050.00'),(30,8,1,1,'4000.00'),(31,8,2,1,'7500.00'),(32,8,3,1,'11000.00'),(33,8,4,1,'6000.00'),(34,9,1,1,'4500.00'),(35,9,2,1,'8500.00'),(36,9,3,1,'12500.00'),(37,9,4,1,'6750.00'),(38,10,1,1,'7000.00'),(39,10,2,1,'14000.00'),(40,10,3,1,'21000.00'),(41,10,4,1,'10500.00');
/*!40000 ALTER TABLE `rooms_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_pricetype`
--

DROP TABLE IF EXISTS `rooms_pricetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_pricetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_pricetype`
--

LOCK TABLES `rooms_pricetype` WRITE;
/*!40000 ALTER TABLE `rooms_pricetype` DISABLE KEYS */;
INSERT INTO `rooms_pricetype` VALUES (1,'Будние дни','сутки'),(2,'Выходные дни','сутки'),(3,'Выходные дни','3 дня / 2 ночи пятница-воскресенье'),(4,'Будние дни','3 дня / 2 ночи воскресенье-пятница');
/*!40000 ALTER TABLE `rooms_pricetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_room`
--

DROP TABLE IF EXISTS `rooms_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `hotel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_room_27a8d263` (`hotel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_room`
--

LOCK TABLES `rooms_room` WRITE;
/*!40000 ALTER TABLE `rooms_room` DISABLE KEYS */;
INSERT INTO `rooms_room` VALUES (1,'Studio','Стандартное размещение\r\n2 гостя\r\nДополнительное размещение\r\n2 гостя\r\nКоличество комнат\r\nОдна кухня-гостиная',1);
/*!40000 ALTER TABLE `rooms_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomconfig`
--

DROP TABLE IF EXISTS `rooms_roomconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomconfig`
--

LOCK TABLES `rooms_roomconfig` WRITE;
/*!40000 ALTER TABLE `rooms_roomconfig` DISABLE KEYS */;
INSERT INTO `rooms_roomconfig` VALUES (1,'Гостиница «Одиссей»'),(2,'Гостиницы «РИФ» и «Шелл»'),(3,'Таунхаусы «Мыс», «Лагуна», «Бриз»'),(4,'Таунхаусы «Нептун», «Посейдон»');
/*!40000 ALTER TABLE `rooms_roomconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roompanorama`
--

DROP TABLE IF EXISTS `rooms_roompanorama`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roompanorama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `panorama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roompanorama_109d8a5f` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roompanorama`
--

LOCK TABLES `rooms_roompanorama` WRITE;
/*!40000 ALTER TABLE `rooms_roompanorama` DISABLE KEYS */;
INSERT INTO `rooms_roompanorama` VALUES (1,1,'Спальня','panoramas/roompanorama/panorama-preview.png');
/*!40000 ALTER TABLE `rooms_roompanorama` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomphoto`
--

DROP TABLE IF EXISTS `rooms_roomphoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomphoto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roomphoto_109d8a5f` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomphoto`
--

LOCK TABLES `rooms_roomphoto` WRITE;
/*!40000 ALTER TABLE `rooms_roomphoto` DISABLE KEYS */;
INSERT INTO `rooms_roomphoto` VALUES (1,1,'Кухоная сторона','images/roomphoto/room-example.png');
/*!40000 ALTER TABLE `rooms_roomphoto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomtype`
--

DROP TABLE IF EXISTS `rooms_roomtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `room_config_id` int(11) NOT NULL,
  `label` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roomtype_42423783` (`room_config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomtype`
--

LOCK TABLES `rooms_roomtype` WRITE;
/*!40000 ALTER TABLE `rooms_roomtype` DISABLE KEYS */;
INSERT INTO `rooms_roomtype` VALUES (1,'Luxe',1,''),(2,'Suite',1,''),(3,'Studio',1,''),(4,'Penthouse',1,''),(5,'2-местный номер',2,''),(6,'3-местный номер',2,''),(7,'4-местный номер',2,''),(8,'4-местный номер',3,''),(9,'5-местный номер',3,''),(10,'6-местный номер',4,'');
/*!40000 ALTER TABLE `rooms_roomtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_season`
--

DROP TABLE IF EXISTS `rooms_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `special_offer_label` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_season`
--

LOCK TABLES `rooms_season` WRITE;
/*!40000 ALTER TABLE `rooms_season` DISABLE KEYS */;
INSERT INTO `rooms_season` VALUES (1,'Проживание зима-весна','2013-01-08','2013-04-30','Вторые сутки \r\nза полцены'),(2,'Проживание лето','2013-06-01','2013-08-31','бесплатные\r\nкомары!');
/*!40000 ALTER TABLE `rooms_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_settings`
--

DROP TABLE IF EXISTS `siteblocks_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_settings`
--

LOCK TABLES `siteblocks_settings` WRITE;
/*!40000 ALTER TABLE `siteblocks_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblocks_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_sitemenu`
--

DROP TABLE IF EXISTS `siteblocks_sitemenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_sitemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siteblocks_sitemenu_63f17a16` (`parent_id`),
  KEY `siteblocks_sitemenu_42b06ff6` (`lft`),
  KEY `siteblocks_sitemenu_6eabc1a6` (`rght`),
  KEY `siteblocks_sitemenu_102f80d8` (`tree_id`),
  KEY `siteblocks_sitemenu_2a8f42e8` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_sitemenu`
--

LOCK TABLES `siteblocks_sitemenu` WRITE;
/*!40000 ALTER TABLE `siteblocks_sitemenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblocks_sitemenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_textblock`
--

DROP TABLE IF EXISTS `siteblocks_textblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_textblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_textblock`
--

LOCK TABLES `siteblocks_textblock` WRITE;
/*!40000 ALTER TABLE `siteblocks_textblock` DISABLE KEYS */;
INSERT INTO `siteblocks_textblock` VALUES (1,'О курорте «аврора клуб»','text_about_aurora','<p><p>Базу отдыха основательно перестроили, обновили и отремонтировали, так как раньше на территории клуба находился детский лагерь «Авроровец». Старые лагерные корпуса превратились в комфортабельные гостиницы, в элегантные таун-хаусы.</p><p>Многие люди, отдыхавшие еще в «Авроровце» приезжают к нам отдохнуть и сегодня. Говорят, что это по прежнему лучшее место для отдыха в Ленинградской области. Приезжайте и вы!</p><br></p>','redactor');
/*!40000 ALTER TABLE `siteblocks_textblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2013-05-29 12:59:42'),(2,'dashboard','0001_initial','2013-05-29 12:59:42'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2013-05-29 12:59:43'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2013-05-29 12:59:43'),(5,'siteblocks','0001_initial','2013-05-29 12:59:43'),(6,'siteblocks','0002_auto__add_textblock','2013-05-29 12:59:43'),(7,'pages','0001_initial','2013-05-29 12:59:43'),(8,'newsboard','0001_initial','2013-05-29 12:59:43'),(9,'rooms','0001_initial','2013-05-29 12:59:44'),(10,'rooms','0002_auto__chg_field_price_price','2013-05-29 14:55:45'),(11,'rooms','0003_auto__add_field_roomtype_label__del_field_room_room_config__add_field_','2013-05-29 15:43:52'),(12,'rooms','0005_auto__del_field_room_room_type__add_field_room_hotel','2013-05-29 16:25:37'),(13,'rooms','0006_auto__del_panorama__del_photo__add_roomphoto__add_roompanorama','2013-05-29 16:44:40');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-29 22:33:49
