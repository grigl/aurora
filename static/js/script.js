Array.min = function( array ){
    return Math.min.apply( Math, array );
};

var Parallax = function() {
	$('.plax-container img').plaxify();
	$.plax.enable();
};

var SuperSlider = function() {
    var sliderWindow = $('.super-slider .slider-window');
    var nextSelector = $('.super-slider .next');
    var previousSelector = $('.super-slider .previous');
    var groupLinksContainer = $('.super-slider .left-menu');
    var controlsContainer = $('.super-slider .previews-container');
    var allDescCont = $('.room-description-container');

    var slider = sliderWindow.bxSlider({
        pager: false,
        controls: false,
        adaptiveHeight: true,
        onSlideBefore: function($slideElement){
            var groupId = $slideElement.attr('data-group');
            var targetImageId = $slideElement.attr('data-image-id');

            SwitchGroupLinks(groupId);
            SwitchGroupControl(groupId, targetImageId);
            SwitchDescription(groupId);
            SwitchPanoramas(groupId);
        }
    });

    var GetSlideCounterByGroup = function(groupId){
        var targetGroupEls = sliderWindow.find('li[data-group="'+groupId+'"]');
        var counts = targetGroupEls.map(function() { return $(this).attr('data-count'); }).get();
        targetCounter = Array.min(counts)-1;

        return targetCounter;
    };

    var GetSlideCounterByImage = function(imageId){
        var targetEl = sliderWindow.find('li[data-image-id="'+imageId+'"]');
        var targetCounter = parseInt(targetEl.attr('data-count'))-1;

        return targetCounter;
    };

    var SwitchGroupInSlider = function(groupId){
        var targetCounter = GetSlideCounterByGroup(groupId);

        slider.goToSlide(targetCounter);
    };

    var SwitchGroupControls = function(groupId){
        var currentGroup = controlsContainer.find('ul.current');
        var targetGroup = controlsContainer.find('ul[data-group="'+groupId+'"]');

        currentGroup.removeClass('current');
        currentGroup.find('li.current').removeClass('current');
        targetGroup.addClass('current');
        targetGroup.find('li').first().addClass('current');
    };

    var SwitchGroupControl = function(groupId, targetImageId){
        var currentLinkContainer = controlsContainer.find('li.current');
        var targetLinkContainer = controlsContainer.find('.preview-link[data-image-id="'+targetImageId+'"]').parents('li');
        var currentGroup = controlsContainer.find('ul.current');
        var targetGroup = controlsContainer.find('ul[data-group="'+groupId+'"]');

        currentGroup.removeClass('current');
        targetGroup.addClass('current');
        currentLinkContainer.removeClass('current');
        targetLinkContainer.addClass('current');

    };

    var SwitchGroupLinks = function(groupId){
        var currentLinkContainer = groupLinksContainer.find('li.current');
        var clickedLinkContainer = groupLinksContainer.find('a.slider-group-link[data-group="'+groupId+'"]').parents('li');

        currentLinkContainer.removeClass('current');          
        clickedLinkContainer.addClass('current');        
    };

    var SwitchDescription = function(groupId){
        var currentDescCont = allDescCont.find('.room-description.current');
        var targetDescCont = allDescCont.find('.room-description[data-group="'+groupId+'"]');

        currentDescCont.removeClass('current');          
        targetDescCont.addClass('current');   
    }

    var SwitchPanoramas = function(groupId){
        var currentPanoCont = $('.panorama-container.current');
        var targetPanoCont = $('.panorama-container[data-group="'+groupId+'"]');

        currentPanoCont.removeClass('current');          
        targetPanoCont.addClass('current');   
    }

    nextSelector.on('click', function(e){
        e.preventDefault();

        slider.goToNextSlide();
    });

    previousSelector.on('click', function(e){
        e.preventDefault();

        slider.goToPrevSlide();
    });

    groupLinksContainer.on('click', '.slider-group-link', function(e){
        e.preventDefault();

        var clickedEl = $(this);    
        var targetGroupId = clickedEl.attr('data-group'); 

        SwitchGroupInSlider(targetGroupId);
        SwitchGroupLinks(targetGroupId);
        SwitchGroupControls(targetGroupId);
    });

    controlsContainer.on('click', '.preview-link', function(e){
        e.preventDefault();

        var targetImageId = $(this).attr('data-image-id');
        var targetCounter = GetSlideCounterByImage(targetImageId);
        var currentLinkContainer = controlsContainer.find('li.current');
        var targetLinkContainer = $(this).parents('li');

        currentLinkContainer.removeClass('current');
        targetLinkContainer.addClass('current');

        slider.goToSlide(targetCounter);
    });
};

var SpecialOffersSlider = function(){
    var sliderWindow = $('.promo-slider ul');
    var nextSelector = $('.promo-slider .next');
    var previousSelector = $('.promo-slider .previous');

    var slider = sliderWindow.bxSlider({
        slideWidth: 320,
        minSlides: 3,
        maxSlides: 3,
        infiniteLoop: false,
        controls: false,
        pager: false
    });

    nextSelector.on('click', function(e){
        e.preventDefault();

        slider.goToNextSlide();
    });

    previousSelector.on('click', function(e){
        e.preventDefault();

        slider.goToPrevSlide();
    });    
};

var SpecialsMenu = function(){
    var el = $('.specials-menu');
    var onBtn = $('.special-offers-link a');
    var offBtn = $('.specials-menu .knob');

    onBtn.on('click', function(e){
        e.preventDefault();
        el.show();
    });

    offBtn.on('click', function(e){
        e.preventDefault();
        el.hide();
    });
};

var SpecialsSlider = function(){
    var sliderWindow = $('.special-offers-slider .image-slider ul');
    var nextSelector = $('.special-offers-slider .image-slider .next');
    var previousSelector = $('.special-offers-slider .image-slider .previous');
    var textContainer = $('.special-offers-slider .text-slider')

    var slider = sliderWindow.bxSlider({
        slideWidth: 320,
        controls: false,
        pager: false,
        onSlideBefore: function($slideElement){
            var targetId = $slideElement.attr('data-id');
            
            textContainer.find('li.current').removeClass('current');
            textContainer.find('li[data-id="'+targetId+'"]').addClass('current');
        }
    });

    nextSelector.on('click', function(e){
        e.preventDefault();

        slider.goToNextSlide();
    });

    previousSelector.on('click', function(e){
        e.preventDefault();

        slider.goToPrevSlide();
    });    
};

var Fancybox = function(){
    $('.fancybox').fancybox({
        padding: 0,
        tpl: {
            closeBtn: '<div class="modal-close"></div>'
        },
        helpers: {
            overlay: {
                opacity: 0.5,
                locked: false,
            }
        }
    });

    $('body').on('submit', '.fancybox-response', function(e){
        e.preventDefault();

        var data = $(this).serialize();
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function(data){
                $.fancybox({
                    padding: 0,
                    tpl: {
                        closeBtn: '<div class="modal-close"></div>'
                    },
                    helpers: {
                        overlay: {
                            opacity: 0.5,
                            locked: false,
                        }
                    },
                    content: data
                });
            }
        });
    });
};

var OrderForm = function(){
    var form = $('.order-form');

    var SeasonSlider = function(){
        var sliderWindow = $('.season-slider ul');
        var nextSelector = $('.season-slider .next');
        var previousSelector = $('.season-slider .previous');

        var slider = sliderWindow.bxSlider({
            slideWidth: 320,
            minSlides: 3,
            maxSlides: 3,
            infiniteLoop: false,
            controls: false,
            pager: false
        });

        nextSelector.on('click', function(e){
            e.preventDefault();

            slider.goToNextSlide();
        });

        previousSelector.on('click', function(e){
            e.preventDefault();

            slider.goToPrevSlide();
        });    
    };

    form.on('click', '.room-config-link', function(e){
        e.preventDefault();

        var container = $(this).parents('.form-room-config');
        var current = $(this).parents('ul').find('li.current');
        var target = $(this).parents('li');
        var id = $(this).attr('data-id');
        var currentOption = $('.room-config-values select option[selected="selected"]');
        var targetOption = $('.room-config-values select option[value="'+id+'"]');

        current.removeClass('current');
        target.addClass('current');
        currentOption.attr('selected', false);
        targetOption.attr('selected', true);

        $('.non-room-fields').show();
        SeasonSlider();
    });

    form.on('click', '.season-link', function(e){
        e.preventDefault();

        var container = $(this).parents('.season-slider');
        var current = $(this).parents('ul').find('li.current');
        var target = $(this).parents('li');
        var id = $(this).attr('data-id');
        var currentOption = $('.season-values select option[selected="selected"]');
        var targetOption = $('.season-values select option[value="'+id+'"]');

        current.removeClass('current');
        target.addClass('current');
        currentOption.attr('selected', false);
        targetOption.attr('selected', true);
    });
};

var Sticker = function(){
    $.each($('.sticker'), function(){
        var s = $(this);
        var pos = s.position();                    
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top) {
                s.addClass("stick");
            } else {
                s.removeClass("stick"); 
            }
        });
    });
}

$(function(){
    Parallax();
    SuperSlider();
    SpecialOffersSlider();
    SpecialsMenu();
    SpecialsSlider();
    Fancybox();
    OrderForm();
    Sticker();
});