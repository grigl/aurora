var SeasonLinks = function(){
    $('.season-box').on('click','.season-link',function(e){
        e.preventDefault();

        var el = $(this);
        var container = $('.season-box');
        var href = el.attr('href');

        $.ajax({
            url: href,
            type: "GET",
            success: function(data) {
                container.html(data);
            }
        });

    });
}

$(function(){
    SeasonLinks();
});