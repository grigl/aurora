var SeasonPage = function(){
    var hash = window.location.hash;
    var container = $('.season-box');

    if ( hash && hash.search('season') != -1 ) {
        var seasonId = hash.replace('#season/','');
        var href = container.data('get-season-url') + '?season=' + seasonId;
        $.ajax({
            url: href,
            type: "GET",
            success: function(data) {
                container.html(data);
            }
        });
    } else {
        var seasonId = container.data('current-season-id');
        var href = container.data('get-season-url') + '?season=' + seasonId;
        $.ajax({
            url: href,
            type: "GET",
            success: function(data) {
                window.location.hash = 'season/' + seasonId;
                container.html(data);
            }
        });
    }
};

var SeasonLinks = function(){
    $('.season-box').on('click','.season-link',function(e){
        e.preventDefault();

        var el = $(this);
        var container = $('.season-box');
        var href = el.attr('href');

        var seasonId = href.substr(href.search('season=')).replace('season=','');

        $.ajax({
            url: href,
            type: "GET",
            success: function(data) {
                window.location.hash = 'season/' + seasonId;

                container.html(data);
            }
        });

    });
}

$(function(){
    SeasonPage();
    SeasonLinks();
});