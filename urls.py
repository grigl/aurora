from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
from django.conf import settings

from apps.urls import urlpatterns as apps_urlpatterns
from views import RoomsPricesView, ChangeRoomPriceView, CreatePricesView, SavePriceTypeView, SaveSeasonView, CreateSeasonView

admin.autodiscover()

urlpatterns = patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),

    # Admin
    url(r'^admin/rooms_prices/$', RoomsPricesView.as_view(), name='admin_rooms_prices_url'),
    url(r'^admin/change_room_price/$', ChangeRoomPriceView.as_view(), name='admin_change_room_price_url'),
    url(r'^admin/create_prices/$', CreatePricesView.as_view(), name='admin_create_prices_url'),
    url(r'^admin/save_price_type/$', SavePriceTypeView.as_view(), name='admin_save_price_type_url'),
    url(r'^admin/save_season/$', SaveSeasonView.as_view(), name='admin_save_season_url'),
    url(r'^admin/create_season/$', CreateSeasonView.as_view(), name='admin_create_season_url'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
	(r'^admin/crop/(?P<app_name>\w+)/(?P<model_name>\w+)/(?P<id>\d+)/$', 'views.crop_image_view'),

     #Redactor
    (r'^upload_img/$', 'views.upload_img'),
    (r'^upload_file/$', 'views.upload_file'),

    # favicon
    (r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/favicon.ico'}),

)

urlpatterns += apps_urlpatterns
