# -*- coding: utf-8 -*-
DATABASE_NAME = u'aurora'
PROJECT_NAME = u'aurora'
SITE_NAME = u'Аврора Клуб'
DEFAULT_FROM_EMAIL = u'support@auroraclub.ru'

from config.base import *

try:
    from config.development import *
except ImportError:
    from config.production import *

TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS += (
    'apps.siteblocks',
    'apps.pages',
    # 'apps.faq',
    # 'apps.slider',
    # 'apps.products',
    # 'apps.newsboard',
    'apps.rooms',
    'apps.services',
    'apps.press',
    'apps.reviews',
    #'apps.orders',
    #'apps.inheritanceUser',
    'sorl.thumbnail',
    'south',
    #'captcha',
)

MIDDLEWARE_CLASSES += (
    'apps.pages.middleware.PageFallbackMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    # 'apps.auth_backends.CustomUserModelBackend',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'apps.pages.context_processors.meta',
    'apps.siteblocks.context_processors.settings',
    'apps.siteblocks.context_processors.textblocks',
    'apps.utils.context_processors.authorization_form',
)
