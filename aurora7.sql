-- MySQL dump 10.13  Distrib 5.6.10, for osx10.6 (x86_64)
--
-- Host: localhost    Database: aurora
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_preferences_dashboard_id_575b1104_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_403f60f` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{\"positions\":{},\"columns\":{},\"disabled\":{},\"collapsed\":{\"module_1\":false}}','rooms-dashboard'),(3,1,'{}','newsboard-dashboard'),(4,1,'{}','press-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(19,'Can add kv store',7,'add_kvstore'),(20,'Can change kv store',7,'change_kvstore'),(21,'Can delete kv store',7,'delete_kvstore'),(22,'Can add migration history',8,'add_migrationhistory'),(23,'Can change migration history',8,'change_migrationhistory'),(24,'Can delete migration history',8,'delete_migrationhistory'),(25,'Can add bookmark',9,'add_bookmark'),(26,'Can change bookmark',9,'change_bookmark'),(27,'Can delete bookmark',9,'delete_bookmark'),(28,'Can add dashboard preferences',10,'add_dashboardpreferences'),(29,'Can change dashboard preferences',10,'change_dashboardpreferences'),(30,'Can delete dashboard preferences',10,'delete_dashboardpreferences'),(31,'Can add menu_item',11,'add_sitemenu'),(32,'Can change menu_item',11,'change_sitemenu'),(33,'Can delete menu_item',11,'delete_sitemenu'),(34,'Can add site_setting',12,'add_settings'),(35,'Can change site_setting',12,'change_settings'),(36,'Can delete site_setting',12,'delete_settings'),(37,'Can add Текстовый блок',13,'add_textblock'),(38,'Can change Текстовый блок',13,'change_textblock'),(39,'Can delete Текстовый блок',13,'delete_textblock'),(40,'Can add page_item',14,'add_page'),(41,'Can change page_item',14,'change_page'),(42,'Can delete page_item',14,'delete_page'),(43,'Can add файл',15,'add_pagedoc'),(44,'Can change файл',15,'change_pagedoc'),(45,'Can delete файл',15,'delete_pagedoc'),(46,'Can add картинка',16,'add_pagepic'),(47,'Can change картинка',16,'change_pagepic'),(48,'Can delete картинка',16,'delete_pagepic'),(49,'Can add meta',17,'add_metadata'),(50,'Can change meta',17,'change_metadata'),(51,'Can delete meta',17,'delete_metadata'),(52,'Can add news_category',18,'add_newscategory'),(53,'Can change news_category',18,'change_newscategory'),(54,'Can delete news_category',18,'delete_newscategory'),(55,'Can add news_item',19,'add_news'),(56,'Can change news_item',19,'change_news'),(57,'Can delete news_item',19,'delete_news'),(64,'Can add конфигурация комнат',22,'add_roomconfig'),(65,'Can change конфигурация комнат',22,'change_roomconfig'),(66,'Can delete конфигурация комнат',22,'delete_roomconfig'),(67,'Can add отель',23,'add_hotel'),(68,'Can change отель',23,'change_hotel'),(69,'Can delete отель',23,'delete_hotel'),(70,'Can add тип комнаты',24,'add_roomtype'),(71,'Can change тип комнаты',24,'change_roomtype'),(72,'Can delete тип комнаты',24,'delete_roomtype'),(73,'Can add комната',25,'add_room'),(74,'Can change комната',25,'change_room'),(75,'Can delete комната',25,'delete_room'),(76,'Can add тип цены',26,'add_pricetype'),(77,'Can change тип цены',26,'change_pricetype'),(78,'Can delete тип цены',26,'delete_pricetype'),(79,'Can add сезон',27,'add_season'),(80,'Can change сезон',27,'change_season'),(81,'Can delete сезон',27,'delete_season'),(82,'Can add цена',28,'add_price'),(83,'Can change цена',28,'change_price'),(84,'Can delete цена',28,'delete_price'),(85,'Can add тип отеля',29,'add_hoteltype'),(86,'Can change тип отеля',29,'change_hoteltype'),(87,'Can delete тип отеля',29,'delete_hoteltype'),(88,'Can add фотография',30,'add_roomphoto'),(89,'Can change фотография',30,'change_roomphoto'),(90,'Can delete фотография',30,'delete_roomphoto'),(91,'Can add панорама',31,'add_roompanorama'),(92,'Can change панорама',31,'change_roompanorama'),(93,'Can delete панорама',31,'delete_roompanorama'),(94,'Can add услуга',32,'add_service'),(95,'Can change услуга',32,'change_service'),(96,'Can delete услуга',32,'delete_service'),(97,'Can add цена услуги',33,'add_serviceprice'),(98,'Can change цена услуги',33,'change_serviceprice'),(99,'Can delete цена услуги',33,'delete_serviceprice'),(100,'Can add пресс релиз',34,'add_pressitem'),(101,'Can change пресс релиз',34,'change_pressitem'),(102,'Can delete пресс релиз',34,'delete_pressitem'),(103,'Can add специальное предложение',35,'add_specialoffer'),(104,'Can change специальное предложение',35,'change_specialoffer'),(105,'Can delete специальное предложение',35,'delete_specialoffer'),(106,'Can add фотоотчет',36,'add_photoreport'),(107,'Can change фотоотчет',36,'change_photoreport'),(108,'Can delete фотоотчет',36,'delete_photoreport'),(109,'Can add фотография',37,'add_pressphoto'),(110,'Can change фотография',37,'change_pressphoto'),(111,'Can delete фотография',37,'delete_pressphoto'),(112,'Can add новость',38,'add_newsitem'),(113,'Can change новость',38,'change_newsitem'),(114,'Can delete новость',38,'delete_newsitem');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'grigl','','','grigl@mail.ru','pbkdf2_sha256$10000$5BwfH7IzPStm$NKjRxOeuCbbzEA0+DDreq6satbA/8bDnHeyJVKge9Hc=',1,1,1,'2013-05-31 13:54:18','2013-05-29 12:59:33');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-05-29 14:08:19',1,22,'1','Гостиница «ОДИССЕЙ»',1,''),(2,'2013-05-29 14:09:20',1,22,'2','Гостиницы «РИФ» и «Шелл»',1,''),(3,'2013-05-29 14:10:37',1,22,'3','Таунхаусы «Мыс», «Лагуна», «Бриз»',1,''),(4,'2013-05-29 14:10:56',1,22,'1','Гостиница «Одиссей»',2,'Изменен title.'),(5,'2013-05-29 14:14:25',1,22,'4','Таунхаусы «Нептун», «Посейдон»',1,''),(6,'2013-05-29 14:48:53',1,26,'1','Будние дни',1,''),(7,'2013-05-29 14:50:05',1,27,'1','Проживание зима-весна',1,''),(8,'2013-05-29 14:52:30',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"999\".'),(9,'2013-05-29 14:53:04',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Удален цена \"1000.00\".'),(10,'2013-05-29 14:56:05',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"2000\".'),(11,'2013-05-29 14:57:21',1,26,'2','Выходные дни сутки',1,''),(12,'2013-05-29 15:02:22',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price_type и price для цена \"3600\".'),(13,'2013-05-29 15:04:23',1,26,'3','Выходные дни 3 дня / 2 ночи пятница-воскресенье',1,''),(14,'2013-05-29 15:07:17',1,26,'4','Будние дни (3 дня / 2 ночи воскресенье-пятница)',1,''),(15,'2013-05-29 15:07:36',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"3600\". Добавлен цена \"5400\". Добавлен цена \"3000\". Изменены price_type и price для цена \"2000\".'),(16,'2013-05-29 15:08:35',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"3000\". Добавлен цена \"6000\". Добавлен цена \"9000\". Добавлен цена \"45000\".'),(17,'2013-05-29 15:08:39',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"4500.00\".'),(18,'2013-05-29 15:13:08',1,24,'3','Studio из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"2600\". Добавлен цена \"5000\". Добавлен цена \"7500\". Добавлен цена \"3900\".'),(19,'2013-05-29 15:15:04',1,24,'4','Penthouse из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"5000\". Добавлен цена \"10000\". Добавлен цена \"15000\". Добавлен цена \"7500\".'),(20,'2013-05-29 15:16:57',1,24,'5','2-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"1500\". Добавлен цена \"2600\". Добавлен цена \"3900\". Добавлен цена \"2250\".'),(21,'2013-05-29 15:19:04',1,24,'6','3-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"2000\". Добавлен цена \"3600\". Добавлен цена \"5400\". Добавлен цена \"3000\".'),(22,'2013-05-29 15:21:18',1,24,'7','4-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"2700\". Добавлен цена \"4800\". Добавлен цена \"7200\". Добавлен цена \"4050\".'),(23,'2013-05-29 15:23:25',1,24,'8','4-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"4000\". Добавлен цена \"7500\". Добавлен цена \"11000\". Добавлен цена \"6000\".'),(24,'2013-05-29 15:24:33',1,24,'9','5-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"4500\". Добавлен цена \"8500\". Добавлен цена \"12500\". Добавлен цена \"6750\".'),(25,'2013-05-29 15:25:28',1,24,'10','6-местный номер из конфигурации Таунхаусы «Нептун», «Посейдон»',2,'Добавлен цена \"7000\". Добавлен цена \"14000\". Добавлен цена \"21000\". Добавлен цена \"10500\".'),(26,'2013-05-29 15:33:47',1,23,'1','Одиссей',1,''),(27,'2013-05-29 15:34:59',1,23,'2','Риф',1,''),(28,'2013-05-29 15:36:02',1,23,'3','Шелл',1,''),(29,'2013-05-29 15:36:17',1,23,'2','Риф',2,'Изменен image.'),(30,'2013-05-29 15:36:27',1,23,'1','Одиссей',2,'Изменен image.'),(31,'2013-05-29 15:51:35',1,29,'1','Гостиница',1,''),(32,'2013-05-29 15:52:40',1,29,'2','Танхаус',1,''),(33,'2013-05-29 15:53:02',1,29,'3','Таунхаус',1,''),(34,'2013-05-29 15:53:17',1,29,'2','Таунхаус',2,'Изменен title.'),(35,'2013-05-29 16:00:14',1,23,'4','Мыс',1,''),(36,'2013-05-29 16:01:01',1,23,'5','Лагуна ',1,''),(37,'2013-05-29 16:01:54',1,23,'6','Бриз',1,''),(38,'2013-05-29 16:07:55',1,23,'7','Посейдон',1,''),(39,'2013-05-29 16:09:09',1,23,'8','Нептун',1,''),(41,'2013-05-29 17:00:08',1,25,'1','Studio',1,''),(42,'2013-05-29 17:01:21',1,25,'1','Studio',2,'Добавлен фотография \"Кухоная сторона\". Добавлен панорама \"Спальня\".'),(43,'2013-05-29 17:04:52',1,13,'1','text_about_aurora',1,''),(44,'2013-05-29 17:05:09',1,13,'1','text_about_aurora',2,'Изменен value.'),(45,'2013-05-29 17:05:13',1,13,'1','text_about_aurora',2,'Изменен value.'),(46,'2013-05-29 17:05:28',1,13,'1','text_about_aurora',2,'Изменен value.'),(47,'2013-05-29 21:55:42',1,27,'1','Проживание зима-весна',2,'Изменен special_offer_label.'),(48,'2013-05-29 22:00:51',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(49,'2013-05-29 22:01:04',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(50,'2013-05-29 22:06:43',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(51,'2013-05-29 22:06:51',1,27,'1','Проживание зима-весна',2,'Изменен date_to.'),(52,'2013-05-30 00:59:00',1,27,'2','Проживание лето',1,''),(53,'2013-05-30 00:59:58',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"6666\".'),(54,'2013-05-30 01:00:38',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены season для цена \"2000.00\". Изменены season для цена \"6666.00\".'),(55,'2013-05-30 01:01:04',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены season для цена \"2000.00\". Изменены season для цена \"6666.00\".'),(56,'2013-05-30 01:22:57',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Удален цена \"6666.00\".'),(57,'2013-05-31 14:07:43',1,29,'3','Таунхаус на 6 человек +2 доп. места',2,'Изменен slug.'),(58,'2013-05-31 14:07:54',1,29,'2','Таунхаус на 4 человек +2 доп. места',2,'Изменен slug.'),(59,'2013-05-31 14:08:01',1,29,'1','Гостиница ',2,'Изменен slug.'),(60,'2013-05-31 14:17:25',1,29,'3','Таунхаус на 6 человек +2 доп. места',2,'Изменен subtype.'),(61,'2013-05-31 14:17:32',1,29,'2','Таунхаус на 4 человек +2 доп. места',2,'Изменен subtype.'),(62,'2013-05-31 14:17:41',1,29,'1','Гостиница ',2,'Изменен subtype.'),(63,'2013-05-31 14:55:20',1,23,'1','Одиссей',2,'Изменен short_description.'),(64,'2013-05-31 14:55:27',1,23,'1','Одиссей',2,'Изменен slug.'),(65,'2013-05-31 15:49:45',1,29,'3','Таунхаус на 6 человек +2 доп. места',2,'Изменен slug.'),(66,'2013-05-31 15:49:54',1,29,'2','Таунхаус на 4 человек +2 доп. места',2,'Изменен slug.'),(67,'2013-05-31 15:50:08',1,29,'1','Гостиница ',2,'Изменен slug.'),(68,'2013-05-31 16:15:12',1,29,'2','Таунхаус на 4 человек',2,'Изменен label.'),(69,'2013-05-31 16:17:57',1,29,'2','Таунхаус на 4 человек +2 доп. места',2,'Изменен label.'),(70,'2013-05-31 16:32:02',1,23,'6','Бриз',2,'Изменен slug.'),(71,'2013-05-31 16:32:15',1,23,'5','Лагуна ',2,'Изменен slug.'),(72,'2013-05-31 16:32:29',1,23,'4','Мыс',2,'Изменен slug.'),(73,'2013-05-31 16:32:40',1,23,'8','Нептун',2,'Изменен slug.'),(74,'2013-05-31 16:32:53',1,23,'7','Посейдон',2,'Изменен slug.'),(75,'2013-05-31 16:32:59',1,23,'2','Риф',2,'Изменен slug.'),(76,'2013-05-31 16:33:08',1,23,'3','Шелл',2,'Изменен slug.'),(77,'2013-05-31 18:28:08',1,29,'3','Таунхаус на 6 человек +2 доп. места',2,'Изменен slug.'),(78,'2013-05-31 18:28:21',1,29,'2','Таунхаус на 4 человек +2 доп. места',2,'Изменен slug.'),(79,'2013-05-31 19:05:15',1,12,'1','summer_or_winter',1,''),(80,'2013-06-01 16:34:11',1,32,'1','Конные прогулки',1,''),(81,'2013-06-01 16:35:22',1,32,'1','Конные прогулки',2,'Добавлен цена услуги \"Конные прогулки в мвнеже\". Добавлен цена услуги \"Конные прогулки на природе\". Добавлен цена услуги \"Конные прогулки под водой\".'),(82,'2013-06-01 16:35:31',1,32,'1','Конные прогулки',2,'Изменены title для цена услуги \"Конные прогулки в манеже\".'),(83,'2013-06-01 16:39:43',1,32,'2','Мини гольф',1,''),(84,'2013-06-01 16:41:29',1,32,'3','Лодки и катамараны',1,''),(85,'2013-06-01 16:47:58',1,32,'4','Прокат спортивного инвентаря',1,''),(86,'2013-06-01 16:49:22',1,32,'5','Пэинтболл',1,''),(87,'2013-06-01 16:54:53',1,32,'6','Дайвинг',1,''),(88,'2013-06-01 16:56:01',1,32,'7','Детская игровая комната',1,''),(89,'2013-06-01 16:59:05',1,32,'8','Анимация',1,''),(90,'2013-06-01 16:59:26',1,32,'5','Пэйнтболл',2,'Изменен title.'),(91,'2013-06-01 17:01:24',1,32,'9','Сауна',1,''),(92,'2013-06-01 17:03:35',1,32,'10','Лыжи, коньки, ватрушки',1,''),(93,'2013-06-01 17:04:43',1,32,'11','Квадроциклы',1,''),(94,'2013-06-01 17:05:42',1,32,'12','Рыбалка',1,''),(95,'2013-06-01 17:06:51',1,32,'13','Веревочный город',1,''),(96,'2013-06-01 17:10:45',1,32,'1','Конные прогулки',2,'Изменен short_description.'),(97,'2013-06-01 17:10:50',1,32,'13','Веревочный город',2,'Изменен short_description.'),(98,'2013-06-01 17:10:58',1,32,'12','Рыбалка',2,'Изменен short_description.'),(99,'2013-06-01 17:11:02',1,32,'11','Квадроциклы',2,'Изменен short_description.'),(100,'2013-06-01 17:11:06',1,32,'10','Лыжи, коньки, ватрушки',2,'Изменен short_description.'),(101,'2013-06-01 17:11:09',1,32,'10','Лыжи, коньки, ватрушки',2,'Ни одно поле не изменено.'),(102,'2013-06-01 17:11:14',1,32,'9','Сауна',2,'Изменен short_description.'),(103,'2013-06-01 17:11:18',1,32,'8','Анимация',2,'Изменен short_description.'),(104,'2013-06-01 17:11:22',1,32,'7','Детская игровая комната',2,'Изменен short_description.'),(105,'2013-06-01 17:11:25',1,32,'7','Детская игровая комната',2,'Ни одно поле не изменено.'),(106,'2013-06-01 17:11:28',1,32,'6','Дайвинг',2,'Изменен short_description.'),(107,'2013-06-01 17:11:32',1,32,'5','Пэйнтболл',2,'Изменен short_description.'),(108,'2013-06-01 17:11:35',1,32,'4','Прокат спортивного инвентаря',2,'Изменен short_description.'),(109,'2013-06-01 17:11:39',1,32,'3','Лодки и катамараны',2,'Изменен short_description.'),(110,'2013-06-01 17:11:43',1,32,'2','Мини гольф',2,'Изменен short_description.'),(111,'2013-06-02 12:35:36',1,25,'2','Luxe',1,''),(112,'2013-06-02 12:41:44',1,23,'1','Одиссей',2,'Изменены description для комната \"Luxe\".'),(113,'2013-06-02 12:45:10',1,25,'2','Luxe',2,'Изменен description.'),(114,'2013-06-02 12:46:00',1,25,'1','Studio',2,'Добавлен фотография \"Комната\". Добавлен фотография \"Коридор\".'),(115,'2013-06-02 12:46:16',1,25,'1','Studio',2,'Изменен description.'),(116,'2013-06-02 12:47:02',1,25,'1','Studio',2,'Изменен description.'),(117,'2013-06-02 13:29:21',1,25,'1','Studio',2,'Добавлен фотография \"Ванная комната\".'),(118,'2013-06-02 18:05:32',1,25,'1','Studio',2,'Изменены image для фотография \"Кухоная сторона\". Изменены image для фотография \"Комната\". Изменены image для фотография \"Коридор\". Изменены image для фотография \"Ванная комната\".'),(119,'2013-06-02 18:05:56',1,25,'2','Luxe',2,'Изменены image для фотография \"Комната\". Изменены image для фотография \"Комната 2\". Изменены image для фотография \"Комната 3\".'),(120,'2013-06-02 19:39:29',1,25,'2','Luxe',2,'Добавлен панорама \"\".'),(121,'2013-06-02 19:39:48',1,25,'1','Studio',2,'Изменены panorama для панорама \"Спальня\".'),(122,'2013-06-02 19:49:24',1,25,'2','Luxe',2,'Изменены title для панорама \"Панорама номера\".'),(123,'2013-06-02 19:53:18',1,25,'2','Luxe',2,'Изменены preview для панорама \"Панорама номера\".'),(124,'2013-06-02 19:53:31',1,25,'1','Studio',2,'Изменены preview для панорама \"Спальня\".'),(125,'2013-06-02 19:59:13',1,25,'1','Studio',2,'Изменены preview для панорама \"Спальня\".'),(126,'2013-06-02 19:59:23',1,25,'2','Luxe',2,'Изменены preview для панорама \"Панорама номера\".'),(127,'2013-06-02 20:26:07',1,32,'1','Конные прогулки',2,'Изменен price_page_description и description.'),(128,'2013-06-02 21:20:56',1,32,'4','Стадион',2,'Изменен title.'),(129,'2013-06-02 21:24:44',1,32,'4','Стадион',2,'Изменен slug.'),(130,'2013-06-03 16:30:51',1,11,'2','Номера',1,''),(131,'2013-06-03 16:32:07',1,11,'1','Номера',3,''),(132,'2013-06-03 16:32:13',1,11,'2','Номера',3,''),(133,'2013-06-03 16:32:25',1,11,'3','Номера',1,''),(134,'2013-06-03 16:32:47',1,11,'4','Рестораны',1,''),(135,'2013-06-03 16:33:24',1,11,'5','Развлечения',1,''),(136,'2013-06-03 16:33:44',1,11,'6','О курорте',1,''),(137,'2013-06-03 16:34:04',1,11,'7','Цены',1,''),(138,'2013-06-03 16:34:21',1,11,'6','О курорте',2,'Изменен url.'),(139,'2013-06-03 16:34:27',1,11,'5','Развлечения',2,'Изменен url.'),(140,'2013-06-03 16:34:33',1,11,'4','Рестораны',2,'Изменен url.'),(141,'2013-06-03 16:34:38',1,11,'3','Номера',2,'Изменен url.'),(142,'2013-06-03 16:34:43',1,11,'7','Цены',2,'Изменен url.'),(143,'2013-06-03 16:37:57',1,11,'8','Отзывы',1,''),(144,'2013-06-03 16:38:04',1,11,'8','Отзывы',2,'Изменен url.'),(145,'2013-06-03 16:38:25',1,11,'8','Отзывы',2,'Изменен order.'),(146,'2013-06-03 16:38:25',1,11,'7','Цены',2,'Изменен order.'),(147,'2013-06-03 16:49:43',1,11,'8','Отзывы',2,'Изменен order.'),(148,'2013-06-03 16:49:43',1,11,'7','Цены',2,'Изменен order.'),(149,'2013-06-03 16:49:43',1,11,'6','О курорте',2,'Изменен order.'),(150,'2013-06-03 16:49:43',1,11,'5','Развлечения',2,'Изменен order.'),(151,'2013-06-03 16:49:43',1,11,'4','Рестораны',2,'Изменен order.'),(152,'2013-06-03 16:49:43',1,11,'3','Номера',2,'Изменен order.'),(153,'2013-06-03 19:53:30',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"666\".'),(154,'2013-06-03 21:57:29',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"345\". Добавлен цена \"123\". Добавлен цена \"65456\".'),(155,'2013-06-03 21:57:53',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"123\". Добавлен цена \"123\". Добавлен цена \"345345\". Добавлен цена \"6666\".'),(156,'2013-06-03 21:59:27',1,24,'3','Studio из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"12\". Добавлен цена \"54345\". Добавлен цена \"654\". Добавлен цена \"86786\".'),(157,'2013-06-03 22:00:00',1,24,'4','Penthouse из конфигурации Гостиница «Одиссей»',2,'Добавлен цена \"123\". Добавлен цена \"34536\". Добавлен цена \"567567\". Добавлен цена \"1233\".'),(158,'2013-06-03 22:00:33',1,24,'5','2-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"12415\". Добавлен цена \"67647\". Добавлен цена \"78978\". Добавлен цена \"2312\".'),(159,'2013-06-03 22:01:23',1,24,'6','3-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"123\". Добавлен цена \"34546\". Добавлен цена \"2342\". Добавлен цена \"12\".'),(160,'2013-06-03 22:01:47',1,24,'7','4-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Добавлен цена \"1234\". Добавлен цена \"234\". Добавлен цена \"45646\". Добавлен цена \"123\".'),(161,'2013-06-03 22:02:18',1,24,'8','4-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"42\". Добавлен цена \"3452\". Добавлен цена \"345\". Добавлен цена \"12341\".'),(162,'2013-06-03 22:03:09',1,24,'9','5-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Добавлен цена \"123\". Добавлен цена \"42345\". Добавлен цена \"654\". Добавлен цена \"1234\".'),(163,'2013-06-03 22:03:49',1,24,'10','6-местный номер из конфигурации Таунхаусы «Нептун», «Посейдон»',2,'Добавлен цена \"234\". Добавлен цена \"563456\". Добавлен цена \"978\". Добавлен цена \"99\".'),(164,'2013-06-03 22:16:13',1,24,'1','Luxe из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"1345.00\". Изменены price для цена \"1666.00\". Изменены price для цена \"1123.00\".'),(165,'2013-06-03 22:16:24',1,24,'2','Suite из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"3123.00\". Изменены price для цена \"3123.00\".'),(166,'2013-06-03 22:16:32',1,24,'3','Studio из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"4312.00\". Изменены price для цена \"4654.00\".'),(167,'2013-06-03 22:16:42',1,24,'4','Penthouse из конфигурации Гостиница «Одиссей»',2,'Изменены price для цена \"1123.00\".'),(168,'2013-06-03 22:16:48',1,24,'5','2-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Ни одно поле не изменено.'),(169,'2013-06-03 22:17:01',1,24,'5','2-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Изменены price для цена \"2415.00\". Изменены price для цена \"8978.00\".'),(170,'2013-06-03 22:17:09',1,24,'6','3-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Изменены price для цена \"6123.00\". Изменены price для цена \"4512.00\".'),(171,'2013-06-03 22:17:15',1,24,'8','4-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Изменены price для цена \"5442.00\". Изменены price для цена \"6345.00\".'),(172,'2013-06-03 22:17:23',1,24,'10','6-местный номер из конфигурации Таунхаусы «Нептун», «Посейдон»',2,'Изменены price для цена \"4234.00\". Изменены price для цена \"7978.00\". Изменены price для цена \"1299.00\".'),(173,'2013-06-03 22:17:29',1,24,'9','5-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Изменены price для цена \"7123.00\". Изменены price для цена \"5654.00\".'),(174,'2013-06-03 22:17:34',1,24,'8','4-местный номер из конфигурации Таунхаусы «Мыс», «Лагуна», «Бриз»',2,'Ни одно поле не изменено.'),(175,'2013-06-03 22:17:41',1,24,'7','4-местный номер из конфигурации Гостиницы «РИФ» и «Шелл»',2,'Изменены price для цена \"3234.00\". Изменены price для цена \"1123.00\".'),(176,'2013-06-03 23:02:21',1,23,'8','Нептун',2,'Изменен image.'),(177,'2013-06-03 23:06:31',1,23,'5','Лагуна ',2,'Изменен image.'),(178,'2013-06-03 23:06:39',1,23,'6','Бриз',2,'Изменен image.'),(179,'2013-06-04 02:39:10',1,18,'1','Специальные предложения',1,''),(180,'2013-06-04 02:50:05',1,19,'1','Новый год и зимние каникулы',1,''),(181,'2013-06-04 02:52:45',1,19,'2','Вторые сутки за полцены',1,''),(182,'2013-06-05 15:21:44',1,35,'7','123',1,''),(183,'2013-06-05 15:22:12',1,35,'7','Новый год и зимние каникулы',2,'Изменен title и text.'),(184,'2013-06-05 15:24:21',1,35,'8','Вторые сутки за полцены',1,''),(185,'2013-06-05 15:25:16',1,35,'9','Конные прогулки',1,''),(186,'2013-06-05 15:29:00',1,35,'10','Супер-Пупер',1,''),(187,'2013-06-05 16:46:13',1,35,'11','Пупер-Супер',1,''),(188,'2013-06-05 17:59:10',1,38,'12','Новость 1',1,''),(189,'2013-06-05 18:00:01',1,36,'13','Фотоотчет 1',1,''),(190,'2013-06-05 18:34:50',1,36,'13','Фотоотчет 1',2,'Ни одно поле не изменено.'),(191,'2013-06-05 18:34:59',1,35,'11','Пупер-Супер',2,'Ни одно поле не изменено.'),(192,'2013-06-05 18:35:03',1,35,'10','Супер-Пупер',2,'Ни одно поле не изменено.'),(193,'2013-06-05 18:35:06',1,35,'9','Конные прогулки',2,'Ни одно поле не изменено.'),(194,'2013-06-05 18:35:08',1,35,'8','Вторые сутки за полцены',2,'Ни одно поле не изменено.'),(195,'2013-06-05 18:35:11',1,35,'7','Новый год и зимние каникулы',2,'Ни одно поле не изменено.'),(196,'2013-06-05 18:35:17',1,38,'12','Новость 1',2,'Ни одно поле не изменено.'),(197,'2013-06-05 18:37:16',1,35,'11','Пупер-Супер',2,'Ни одно поле не изменено.'),(198,'2013-06-05 18:37:20',1,35,'10','Супер-Пупер',2,'Ни одно поле не изменено.'),(199,'2013-06-05 18:37:23',1,35,'9','Конные прогулки',2,'Ни одно поле не изменено.'),(200,'2013-06-05 18:37:26',1,35,'8','Вторые сутки за полцены',2,'Ни одно поле не изменено.'),(201,'2013-06-05 18:37:29',1,35,'7','Новый год и зимние каникулы',2,'Ни одно поле не изменено.'),(202,'2013-06-05 18:41:43',1,35,'11','Пупер-Супер',2,'Ни одно поле не изменено.'),(203,'2013-06-05 18:41:46',1,35,'10','Супер-Пупер',2,'Ни одно поле не изменено.'),(204,'2013-06-05 18:41:48',1,35,'9','Конные прогулки',2,'Ни одно поле не изменено.'),(205,'2013-06-05 18:41:51',1,35,'8','Вторые сутки за полцены',2,'Ни одно поле не изменено.'),(206,'2013-06-05 18:41:53',1,35,'7','Новый год и зимние каникулы',2,'Ни одно поле не изменено.'),(207,'2013-06-05 18:41:59',1,36,'13','Фотоотчет 1',2,'Ни одно поле не изменено.'),(208,'2013-06-05 18:42:05',1,38,'12','Новость 1',2,'Ни одно поле не изменено.'),(209,'2013-06-05 18:48:56',1,38,'12','Новость 1',2,'Ни одно поле не изменено.'),(210,'2013-06-05 18:49:03',1,36,'13','Фотоотчет 1',2,'Ни одно поле не изменено.'),(211,'2013-06-05 18:49:11',1,35,'11','Пупер-Супер',2,'Ни одно поле не изменено.'),(212,'2013-06-05 18:49:14',1,35,'10','Супер-Пупер',2,'Ни одно поле не изменено.'),(213,'2013-06-05 18:49:17',1,35,'9','Конные прогулки',2,'Ни одно поле не изменено.'),(214,'2013-06-05 18:49:20',1,35,'8','Вторые сутки за полцены',2,'Ни одно поле не изменено.'),(215,'2013-06-05 18:49:22',1,35,'7','Новый год и зимние каникулы',2,'Ни одно поле не изменено.'),(216,'2013-06-05 18:51:30',1,35,'14','qwert',1,''),(217,'2013-06-05 18:53:12',1,38,'15','352345',1,''),(218,'2013-06-05 18:54:07',1,38,'12','Новость 1',2,'Ни одно поле не изменено.'),(219,'2013-06-05 18:54:13',1,36,'13','Фотоотчет 1',2,'Ни одно поле не изменено.'),(220,'2013-06-05 18:54:27',1,35,'14','qwert',3,''),(221,'2013-06-05 18:54:31',1,35,'11','Пупер-Супер',2,'Ни одно поле не изменено.'),(222,'2013-06-05 18:54:33',1,35,'10','Супер-Пупер',2,'Ни одно поле не изменено.'),(223,'2013-06-05 18:54:36',1,35,'9','Конные прогулки',2,'Ни одно поле не изменено.'),(224,'2013-06-05 18:54:39',1,35,'8','Вторые сутки за полцены',2,'Ни одно поле не изменено.'),(225,'2013-06-05 18:54:41',1,35,'7','Новый год и зимние каникулы',2,'Ни одно поле не изменено.'),(226,'2013-06-05 18:56:22',1,38,'15','352345',3,''),(227,'2013-06-05 18:56:38',1,38,'12','Новость 1',2,'Изменен press_type и press_type_slug.'),(228,'2013-06-05 18:56:46',1,36,'13','Фотоотчет 1',2,'Ни одно поле не изменено.');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'log entry','admin','logentry'),(7,'kv store','thumbnail','kvstore'),(8,'migration history','south','migrationhistory'),(9,'bookmark','menu','bookmark'),(10,'dashboard preferences','dashboard','dashboardpreferences'),(11,'menu_item','siteblocks','sitemenu'),(12,'site_setting','siteblocks','settings'),(13,'Текстовый блок','siteblocks','textblock'),(14,'page_item','pages','page'),(15,'файл','pages','pagedoc'),(16,'картинка','pages','pagepic'),(17,'meta','pages','metadata'),(18,'news_category','newsboard','newscategory'),(19,'news_item','newsboard','news'),(22,'конфигурация комнат','rooms','roomconfig'),(23,'отель','rooms','hotel'),(24,'тип комнаты','rooms','roomtype'),(25,'комната','rooms','room'),(26,'тип цены','rooms','pricetype'),(27,'сезон','rooms','season'),(28,'цена','rooms','price'),(29,'тип отеля','rooms','hoteltype'),(30,'фотография','rooms','roomphoto'),(31,'панорама','rooms','roompanorama'),(32,'услуга','services','service'),(33,'цена услуги','services','serviceprice'),(34,'пресс релиз','press','pressitem'),(35,'специальное предложение','press','specialoffer'),(36,'фотоотчет','press','photoreport'),(37,'фотография','press','pressphoto'),(38,'новость','press','newsitem');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('679c10daab64cb733912b8cfa8759296','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-06-12 13:44:36'),('041a8d6e17d255ccde3c0ffa21873381','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-06-14 13:54:18');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_news`
--

DROP TABLE IF EXISTS `newsboard_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `text` longtext NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `date_add` date NOT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsboard_news_42dc49bc` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_news`
--

LOCK TABLES `newsboard_news` WRITE;
/*!40000 ALTER TABLE `newsboard_news` DISABLE KEYS */;
INSERT INTO `newsboard_news` VALUES (1,'Новый год и зимние каникулы',1,'images/news/promo.png','<p>Новый год и зимние каникулы<br></p>','<p>Новый год и зимние каникулы<br></p>',1,'2013-06-04','2013-08-04'),(2,'Вторые сутки за полцены',1,'images/news/promo_3.png','<p>Вторые сутки за полцены<br></p>','<p>Вторые сутки за полцены<br></p>',1,'2013-06-04',NULL);
/*!40000 ALTER TABLE `newsboard_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_newscategory`
--

DROP TABLE IF EXISTS `newsboard_newscategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_newscategory`
--

LOCK TABLES `newsboard_newscategory` WRITE;
/*!40000 ALTER TABLE `newsboard_newscategory` DISABLE KEYS */;
INSERT INTO `newsboard_newscategory` VALUES (1,'Специальные предложения',0);
/*!40000 ALTER TABLE `newsboard_newscategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_metadata`
--

DROP TABLE IF EXISTS `pages_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_metadata`
--

LOCK TABLES `pages_metadata` WRITE;
/*!40000 ALTER TABLE `pages_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_page`
--

DROP TABLE IF EXISTS `pages_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_at_menu` tinyint(1) NOT NULL,
  `is_at_footer_menu` tinyint(1) NOT NULL,
  `template` varchar(100) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `pages_page_63f17a16` (`parent_id`),
  KEY `pages_page_42b06ff6` (`lft`),
  KEY `pages_page_6eabc1a6` (`rght`),
  KEY `pages_page_102f80d8` (`tree_id`),
  KEY `pages_page_2a8f42e8` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_page`
--

LOCK TABLES `pages_page` WRITE;
/*!40000 ALTER TABLE `pages_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagedoc`
--

DROP TABLE IF EXISTS `pages_pagedoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagedoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagedoc_32d04bc7` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagedoc`
--

LOCK TABLES `pages_pagedoc` WRITE;
/*!40000 ALTER TABLE `pages_pagedoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagedoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagepic`
--

DROP TABLE IF EXISTS `pages_pagepic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagepic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagepic_32d04bc7` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagepic`
--

LOCK TABLES `pages_pagepic` WRITE;
/*!40000 ALTER TABLE `pages_pagepic` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagepic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_newsitem`
--

DROP TABLE IF EXISTS `press_newsitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_newsitem` (
  `pressitem_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`pressitem_ptr_id`),
  CONSTRAINT `pressitem_ptr_id_refs_id_483b44933917f6a4` FOREIGN KEY (`pressitem_ptr_id`) REFERENCES `press_pressitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_newsitem`
--

LOCK TABLES `press_newsitem` WRITE;
/*!40000 ALTER TABLE `press_newsitem` DISABLE KEYS */;
INSERT INTO `press_newsitem` VALUES (12);
/*!40000 ALTER TABLE `press_newsitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_photoreport`
--

DROP TABLE IF EXISTS `press_photoreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_photoreport` (
  `pressitem_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`pressitem_ptr_id`),
  CONSTRAINT `pressitem_ptr_id_refs_id_2e23f6ae8be79af1` FOREIGN KEY (`pressitem_ptr_id`) REFERENCES `press_pressitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_photoreport`
--

LOCK TABLES `press_photoreport` WRITE;
/*!40000 ALTER TABLE `press_photoreport` DISABLE KEYS */;
INSERT INTO `press_photoreport` VALUES (13);
/*!40000 ALTER TABLE `press_photoreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_pressitem`
--

DROP TABLE IF EXISTS `press_pressitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_pressitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `short_text` longtext NOT NULL,
  `text` longtext NOT NULL,
  `date_add` date NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `press_type` varchar(150) NOT NULL,
  `press_type_slug` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_pressitem`
--

LOCK TABLES `press_pressitem` WRITE;
/*!40000 ALTER TABLE `press_pressitem` DISABLE KEYS */;
INSERT INTO `press_pressitem` VALUES (7,'Новый год и зимние каникулы','','Новый год\r\nи зимние\r\nканикулы','2013-06-05',1,'специальное предложение','special_offer'),(8,'Вторые сутки за полцены','','Вторые сутки\r\nза полцены','2013-06-05',1,'специальное предложение','special_offer'),(9,'Конные прогулки','','Конные\r\nпрогулки','2013-06-05',1,'специальное предложение','special_offer'),(10,'Супер-Пупер','','Супер-Пупер','2013-06-05',1,'специальное предложение','special_offer'),(11,'Пупер-Супер','','Пупер-Супер','2013-06-05',1,'специальное предложение','special_offer'),(12,'Новость 1','','текст','2013-06-05',1,'новость','news'),(13,'Фотоотчет 1','','Фотоотчет 1','2013-06-05',1,'фотоотчет','photoreport');
/*!40000 ALTER TABLE `press_pressitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_pressphoto`
--

DROP TABLE IF EXISTS `press_pressphoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_pressphoto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_report_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `press_pressphoto_55f0450d` (`photo_report_id`),
  CONSTRAINT `photo_report_id_refs_pressitem_ptr_id_630902dbde5c19b3` FOREIGN KEY (`photo_report_id`) REFERENCES `press_photoreport` (`pressitem_ptr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_pressphoto`
--

LOCK TABLES `press_pressphoto` WRITE;
/*!40000 ALTER TABLE `press_pressphoto` DISABLE KEYS */;
INSERT INTO `press_pressphoto` VALUES (1,13,'123','images/press/pressphoto/boat.png'),(2,13,'234','images/press/pressphoto/globe.png'),(3,13,'345','images/press/pressphoto/mini-logo.png');
/*!40000 ALTER TABLE `press_pressphoto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_specialoffer`
--

DROP TABLE IF EXISTS `press_specialoffer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_specialoffer` (
  `pressitem_ptr_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `date_from` date,
  `date_to` date,
  PRIMARY KEY (`pressitem_ptr_id`),
  CONSTRAINT `pressitem_ptr_id_refs_id_47ba9968173af055` FOREIGN KEY (`pressitem_ptr_id`) REFERENCES `press_pressitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_specialoffer`
--

LOCK TABLES `press_specialoffer` WRITE;
/*!40000 ALTER TABLE `press_specialoffer` DISABLE KEYS */;
INSERT INTO `press_specialoffer` VALUES (7,'images/press/specialoffer/promo_6.png',NULL,NULL),(8,'images/press/specialoffer/promo_7.png','2013-06-05','2013-09-05'),(9,'images/press/specialoffer/promo_8.png',NULL,NULL),(10,'images/press/specialoffer/promo_9.png','2013-03-01','2011-10-09'),(11,'images/press/specialoffer/promo_10.png',NULL,NULL);
/*!40000 ALTER TABLE `press_specialoffer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_hotel`
--

DROP TABLE IF EXISTS `rooms_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `hotel_type_id` int(11) NOT NULL,
  `room_config_id` int(11) NOT NULL,
  `short_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_hotel_42423783` (`room_config_id`),
  KEY `rooms_hotel_a951d5d6` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_hotel`
--

LOCK TABLES `rooms_hotel` WRITE;
/*!40000 ALTER TABLE `rooms_hotel` DISABLE KEYS */;
INSERT INTO `rooms_hotel` VALUES (1,'Одиссей',1,1,'Новая гостиница категории «Люкс» на берегу большого озера с номерами повышенной комфортности.','Гостиница Одиссей','images/hotel/hotel-2.png','odyssey'),(2,'Риф',1,2,'Гостиница Риф','Гостиница Риф','images/hotel/hotel-3.png','rif'),(3,'Шелл',1,2,'Гостиница Шелл','Гостиница Шелл','images/hotel/hotel_2.png','shell'),(4,'Мыс',2,3,'Таунхаус Мыс','Таунхаус Мыс','images/hotel/townhouse.png','mis'),(5,'Лагуна ',2,3,'Таунхаус Лагуна','Таунхаус Лагуна','images/hotel/5/townhouse.png','laguna'),(6,'Бриз',2,3,'Таунхаус Бриз','Таунхаус Бриз','images/hotel/6/townhouse.png','briz'),(7,'Посейдон',3,4,'Таунхаус Посейдон','Таунхаус Посейдон','images/hotel/townhouse2.png','poseydon'),(8,'Нептун',3,4,'Таунхаус Нептун','Таунхаус Нептун','images/hotel/8/townhouse2.png','neptun');
/*!40000 ALTER TABLE `rooms_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_hoteltype`
--

DROP TABLE IF EXISTS `rooms_hoteltype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_hoteltype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `label` varchar(150) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_hoteltype_a951d5d6` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_hoteltype`
--

LOCK TABLES `rooms_hoteltype` WRITE;
/*!40000 ALTER TABLE `rooms_hoteltype` DISABLE KEYS */;
INSERT INTO `rooms_hoteltype` VALUES (1,'Гостиница','','hotel'),(2,'Таунхаус','на 4 человек +2 доп. места','townhouse_4'),(3,'Таунхаус','на 6 человек +2 доп. места','townhouse_6');
/*!40000 ALTER TABLE `rooms_hoteltype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_price`
--

DROP TABLE IF EXISTS `rooms_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_price_4e1f844b` (`room_type_id`),
  KEY `rooms_price_39d67f40` (`price_type_id`),
  KEY `rooms_price_23579bcd` (`season_id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_price`
--

LOCK TABLES `rooms_price` WRITE;
/*!40000 ALTER TABLE `rooms_price` DISABLE KEYS */;
INSERT INTO `rooms_price` VALUES (2,1,1,1,2000.00),(3,1,2,1,3600.00),(4,1,3,1,5400.00),(5,1,4,1,3000.00),(6,2,1,1,3000.00),(7,2,2,1,6000.00),(8,2,3,1,9000.00),(9,2,4,1,4500.00),(10,3,1,1,2600.00),(11,3,2,1,5000.00),(12,3,3,1,7500.00),(13,3,4,1,3900.00),(14,4,1,1,5000.00),(15,4,2,1,10000.00),(16,4,3,1,15000.00),(17,4,4,1,7500.00),(18,5,1,1,1500.00),(19,5,2,1,2600.00),(20,5,3,1,3900.00),(21,5,4,1,2250.00),(22,6,1,1,2000.00),(23,6,2,1,3600.00),(24,6,3,1,5400.00),(25,6,4,1,3000.00),(26,7,1,1,2700.00),(27,7,2,1,4800.00),(28,7,3,1,7200.00),(29,7,4,1,4050.00),(30,8,1,1,4000.00),(31,8,2,1,7500.00),(32,8,3,1,11000.00),(33,8,4,1,6000.00),(34,9,1,1,4500.00),(35,9,2,1,8500.00),(36,9,3,1,12500.00),(37,9,4,1,6750.00),(38,10,1,1,7000.00),(39,10,2,1,14000.00),(40,10,3,1,21000.00),(41,10,4,1,10500.00),(43,1,2,2,1666.00),(44,1,1,2,1345.00),(45,1,3,2,1123.00),(46,1,4,2,65456.00),(47,2,1,2,3123.00),(48,2,2,2,3123.00),(49,2,3,2,345345.00),(50,2,4,2,6666.00),(51,3,1,2,4312.00),(52,3,2,2,54345.00),(53,3,3,2,4654.00),(54,3,4,2,86786.00),(55,4,1,2,1123.00),(56,4,2,2,34536.00),(57,4,3,2,567567.00),(58,4,4,2,1233.00),(59,5,1,2,2415.00),(60,5,2,2,67647.00),(61,5,3,2,8978.00),(62,5,4,2,2312.00),(63,6,1,2,6123.00),(64,6,2,2,34546.00),(65,6,3,2,2342.00),(66,6,4,2,4512.00),(67,7,1,2,1234.00),(68,7,2,2,3234.00),(69,7,3,2,45646.00),(70,7,4,2,1123.00),(71,8,1,2,5442.00),(72,8,2,2,3452.00),(73,8,3,2,6345.00),(74,8,4,2,12341.00),(75,9,1,2,7123.00),(76,9,2,2,42345.00),(77,9,3,2,5654.00),(78,9,4,2,1234.00),(79,10,1,2,4234.00),(80,10,2,2,563456.00),(81,10,3,2,7978.00),(82,10,4,2,1299.00);
/*!40000 ALTER TABLE `rooms_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_pricetype`
--

DROP TABLE IF EXISTS `rooms_pricetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_pricetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_pricetype`
--

LOCK TABLES `rooms_pricetype` WRITE;
/*!40000 ALTER TABLE `rooms_pricetype` DISABLE KEYS */;
INSERT INTO `rooms_pricetype` VALUES (1,'Будние дни','сутки'),(2,'Выходные дни','сутки'),(3,'Выходные дни','3 дня / 2 ночи пятница-воскресенье'),(4,'Будние дни','3 дня / 2 ночи воскресенье-пятница');
/*!40000 ALTER TABLE `rooms_pricetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_room`
--

DROP TABLE IF EXISTS `rooms_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `hotel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_room_27a8d263` (`hotel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_room`
--

LOCK TABLES `rooms_room` WRITE;
/*!40000 ALTER TABLE `rooms_room` DISABLE KEYS */;
INSERT INTO `rooms_room` VALUES (1,'Studio','Стандартное размещение\r\n<p>2 гостя&nbsp;</p>\r\n\r\n<p>Дополнительное размещение&nbsp;</p>\r\n\r\n<p>2 гостя&nbsp;</p>\r\n\r\n<p>Количество комнат</p>\r\n\r\n<p>Одна кухня-гостиная </p>',1),(2,'Luxe','Стандартное размещение&nbsp;<div>2 гостя&nbsp;</div><div>Дополнительное размещение&nbsp;</div><div>2 гостя&nbsp;</div><div>Количество комнат&nbsp;</div><div>Одна кухня-гостиная\r\n</div>',1);
/*!40000 ALTER TABLE `rooms_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomconfig`
--

DROP TABLE IF EXISTS `rooms_roomconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomconfig`
--

LOCK TABLES `rooms_roomconfig` WRITE;
/*!40000 ALTER TABLE `rooms_roomconfig` DISABLE KEYS */;
INSERT INTO `rooms_roomconfig` VALUES (1,'Гостиница «Одиссей»'),(2,'Гостиницы «РИФ» и «Шелл»'),(3,'Таунхаусы «Мыс», «Лагуна», «Бриз»'),(4,'Таунхаусы «Нептун», «Посейдон»');
/*!40000 ALTER TABLE `rooms_roomconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roompanorama`
--

DROP TABLE IF EXISTS `rooms_roompanorama`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roompanorama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `panorama` varchar(100) NOT NULL,
  `preview` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roompanorama_109d8a5f` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roompanorama`
--

LOCK TABLES `rooms_roompanorama` WRITE;
/*!40000 ALTER TABLE `rooms_roompanorama` DISABLE KEYS */;
INSERT INTO `rooms_roompanorama` VALUES (1,1,'Спальня','panoramas/roompanorama/room-studio.png','images/roompanorama/room-studio_1.png'),(2,2,'Панорама номера','panoramas/roompanorama/room-luxe.png','images/roompanorama/room-luxe_1.png');
/*!40000 ALTER TABLE `rooms_roompanorama` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomphoto`
--

DROP TABLE IF EXISTS `rooms_roomphoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomphoto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roomphoto_109d8a5f` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomphoto`
--

LOCK TABLES `rooms_roomphoto` WRITE;
/*!40000 ALTER TABLE `rooms_roomphoto` DISABLE KEYS */;
INSERT INTO `rooms_roomphoto` VALUES (1,1,'Кухоная сторона','images/roomphoto/room-example1.jpg'),(2,2,'Комната','images/roomphoto/room-example5.jpg'),(3,2,'Комната 2','images/roomphoto/room-example6.jpg'),(4,2,'Комната 3','images/roomphoto/room-example7.jpg'),(5,1,'Комната','images/roomphoto/room-example2.jpg'),(6,1,'Коридор','images/roomphoto/room-example3.jpg'),(7,1,'Ванная комната','images/roomphoto/room-example4.jpg');
/*!40000 ALTER TABLE `rooms_roomphoto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_roomtype`
--

DROP TABLE IF EXISTS `rooms_roomtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_roomtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `room_config_id` int(11) NOT NULL,
  `label` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_roomtype_42423783` (`room_config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_roomtype`
--

LOCK TABLES `rooms_roomtype` WRITE;
/*!40000 ALTER TABLE `rooms_roomtype` DISABLE KEYS */;
INSERT INTO `rooms_roomtype` VALUES (1,'Luxe',1,''),(2,'Suite',1,''),(3,'Studio',1,''),(4,'Penthouse',1,''),(5,'2-местный номер',2,''),(6,'3-местный номер',2,''),(7,'4-местный номер',2,''),(8,'4-местный номер',3,''),(9,'5-местный номер',3,''),(10,'6-местный номер',4,'');
/*!40000 ALTER TABLE `rooms_roomtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_season`
--

DROP TABLE IF EXISTS `rooms_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `special_offer_label` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_season`
--

LOCK TABLES `rooms_season` WRITE;
/*!40000 ALTER TABLE `rooms_season` DISABLE KEYS */;
INSERT INTO `rooms_season` VALUES (1,'Проживание зима-весна','2013-01-08','2013-04-30','Вторые сутки \r\nза полцены'),(2,'Проживание лето','2013-06-01','2013-08-31','бесплатные\r\nкомары!');
/*!40000 ALTER TABLE `rooms_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_service`
--

DROP TABLE IF EXISTS `services_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `short_description` longtext NOT NULL,
  `price_page_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_service_a951d5d6` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_service`
--

LOCK TABLES `services_service` WRITE;
/*!40000 ALTER TABLE `services_service` DISABLE KEYS */;
INSERT INTO `services_service` VALUES (1,'Конные прогулки','horse_riding','images/service/service-horce.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности'),(2,'Мини гольф','minigolf','images/service/service_mini_golf.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(3,'Лодки и катамараны','boats','images/service/service_boats.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(4,'Стадион','stadio','images/service/service-sport_rent.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(5,'Пэйнтболл','paintball','images/service/service-paintball.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(6,'Дайвинг','diving','images/service/service-diving.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(7,'Детская игровая комната','childroom','images/service/service-childroom.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(8,'Анимация','animation','images/service/service-animation.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(9,'Сауна','sauna','images/service/service-sauna.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(10,'Лыжи, коньки, ватрушки','skees','images/service/service-skees.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(11,'Квадроциклы','quadro','images/service/service-quadro.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(12,'Рыбалка','fishing','images/service/fishing.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','',''),(13,'Веревочный город','rope_city','images/service/service-rope-city.png','На базе имеется конюшня, где вы можете полюбоваться на красивых лошадей, а также прокатиться верхом, осматривая окрестности','','');
/*!40000 ALTER TABLE `services_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_serviceprice`
--

DROP TABLE IF EXISTS `services_serviceprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_serviceprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_serviceprice_90e28c3e` (`service_id`),
  CONSTRAINT `service_id_refs_id_6bf8929b44e414b1` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_serviceprice`
--

LOCK TABLES `services_serviceprice` WRITE;
/*!40000 ALTER TABLE `services_serviceprice` DISABLE KEYS */;
INSERT INTO `services_serviceprice` VALUES (1,1,'Конные прогулки в манеже',123.00,'1 час'),(2,1,'Конные прогулки на природе',234.00,'1 час'),(3,1,'Конные прогулки под водой',345.00,'1 час');
/*!40000 ALTER TABLE `services_serviceprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_settings`
--

DROP TABLE IF EXISTS `siteblocks_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_settings`
--

LOCK TABLES `siteblocks_settings` WRITE;
/*!40000 ALTER TABLE `siteblocks_settings` DISABLE KEYS */;
INSERT INTO `siteblocks_settings` VALUES (1,'Зима или Лето','summer_or_winter','winter','input');
/*!40000 ALTER TABLE `siteblocks_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_sitemenu`
--

DROP TABLE IF EXISTS `siteblocks_sitemenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_sitemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siteblocks_sitemenu_63f17a16` (`parent_id`),
  KEY `siteblocks_sitemenu_42b06ff6` (`lft`),
  KEY `siteblocks_sitemenu_6eabc1a6` (`rght`),
  KEY `siteblocks_sitemenu_102f80d8` (`tree_id`),
  KEY `siteblocks_sitemenu_2a8f42e8` (`level`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_sitemenu`
--

LOCK TABLES `siteblocks_sitemenu` WRITE;
/*!40000 ALTER TABLE `siteblocks_sitemenu` DISABLE KEYS */;
INSERT INTO `siteblocks_sitemenu` VALUES (4,'Рестораны','',NULL,'/restraunts/',6,1,1,2,4,0),(3,'Номера','',NULL,'/rooms/',5,1,1,2,3,0),(5,'Развлечения','',NULL,'/services/',7,1,1,2,5,0),(6,'О курорте','',NULL,'/about/',8,1,1,2,6,0),(7,'Цены','',NULL,'/prices/',9,1,1,2,5,0),(8,'Отзывы','',NULL,'/reviews/',10,1,1,2,3,0);
/*!40000 ALTER TABLE `siteblocks_sitemenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_textblock`
--

DROP TABLE IF EXISTS `siteblocks_textblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_textblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_textblock`
--

LOCK TABLES `siteblocks_textblock` WRITE;
/*!40000 ALTER TABLE `siteblocks_textblock` DISABLE KEYS */;
INSERT INTO `siteblocks_textblock` VALUES (1,'О курорте «аврора клуб»','text_about_aurora','<p><p>Базу отдыха основательно перестроили, обновили и отремонтировали, так как раньше на территории клуба находился детский лагерь «Авроровец». Старые лагерные корпуса превратились в комфортабельные гостиницы, в элегантные таун-хаусы.</p><p>Многие люди, отдыхавшие еще в «Авроровце» приезжают к нам отдохнуть и сегодня. Говорят, что это по прежнему лучшее место для отдыха в Ленинградской области. Приезжайте и вы!</p><br></p>','redactor');
/*!40000 ALTER TABLE `siteblocks_textblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2013-05-29 12:59:42'),(2,'dashboard','0001_initial','2013-05-29 12:59:42'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2013-05-29 12:59:43'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2013-05-29 12:59:43'),(5,'siteblocks','0001_initial','2013-05-29 12:59:43'),(6,'siteblocks','0002_auto__add_textblock','2013-05-29 12:59:43'),(7,'pages','0001_initial','2013-05-29 12:59:43'),(8,'newsboard','0001_initial','2013-05-29 12:59:43'),(9,'rooms','0001_initial','2013-05-29 12:59:44'),(10,'rooms','0002_auto__chg_field_price_price','2013-05-29 14:55:45'),(11,'rooms','0003_auto__add_field_roomtype_label__del_field_room_room_config__add_field_','2013-05-29 15:43:52'),(12,'rooms','0005_auto__del_field_room_room_type__add_field_room_hotel','2013-05-29 16:25:37'),(13,'rooms','0006_auto__del_panorama__del_photo__add_roomphoto__add_roompanorama','2013-05-29 16:44:40'),(14,'rooms','0007_auto__add_field_hoteltype_slug__add_field_hotel_slug__chg_field_season','2013-05-31 13:54:05'),(15,'rooms','0008_auto__del_field_hoteltype_slug__add_field_hoteltype_subtype','2013-05-31 14:13:19'),(16,'rooms','0009_auto__add_field_hoteltype_slug','2013-05-31 15:49:01'),(17,'services','0001_initial','2013-06-01 16:22:16'),(18,'rooms','0010_auto__add_field_roompanorama_preview','2013-06-02 19:52:45'),(19,'newsboard','0002_auto__del_field_news_on_main_page__add_field_news_date_end__chg_field_','2013-06-04 02:48:18'),(20,'newsboard','0003_auto__chg_field_news_date_end','2013-06-04 02:52:22'),(21,'press','0001_initial','2013-06-05 14:15:32'),(22,'press','0002_auto__chg_field_specialoffer_date_from__chg_field_specialoffer_date_to','2013-06-05 15:13:26'),(23,'press','0003_auto__add_newsitem','2013-06-05 16:34:44'),(24,'press','0004_auto__add_field_pressitem_type','2013-06-05 18:34:34'),(25,'press','0005_auto__chg_field_pressitem_type','2013-06-05 18:40:32'),(26,'press','0006_auto__del_field_pressitem_type','2013-06-05 18:43:19'),(27,'press','0007_auto__add_field_pressitem_press_type__add_field_pressitem_press_type_s','2013-06-05 18:47:35');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||68b2b6d6f49b81541ede2f7f0a8e246e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/hotel-2.png\", \"size\": [180, 95]}'),('sorl-thumbnail||image||8c10e056a941d1fbfa4b60ee6f821714','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/66/40/6640894992c73de60b883e2894accd07.jpg\", \"size\": [119, 63]}'),('sorl-thumbnail||thumbnails||68b2b6d6f49b81541ede2f7f0a8e246e','[\"8c10e056a941d1fbfa4b60ee6f821714\", \"813bf1c3db237c29a8ef6d6aae779c40\"]'),('sorl-thumbnail||image||32d2b442cf4c6cabfaa27aa27dbe6911','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/hotel-3.png\", \"size\": [180, 113]}'),('sorl-thumbnail||image||a3fa7bdda7cb5654403ed8388a13d7f8','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/51/68/5168865d7400511f49b6f9709623222f.jpg\", \"size\": [119, 75]}'),('sorl-thumbnail||thumbnails||32d2b442cf4c6cabfaa27aa27dbe6911','[\"a3fa7bdda7cb5654403ed8388a13d7f8\", \"7e9405e7db622ed255f223d3cc5fb870\"]'),('sorl-thumbnail||image||221cd0b72fd5908c8ee11393d3cb6f92','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/hotel_2.png\", \"size\": [178, 110]}'),('sorl-thumbnail||image||e40a8a11daaf197e725833b3b66220f3','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/66/9e/669efc99578888f6f2f8ef204ddf7d68.jpg\", \"size\": [120, 74]}'),('sorl-thumbnail||thumbnails||221cd0b72fd5908c8ee11393d3cb6f92','[\"a4a25cce60b167d8ac1319dd70cfdd5c\", \"e40a8a11daaf197e725833b3b66220f3\"]'),('sorl-thumbnail||image||f1d7cebdcf07e36330fdb9e06806e059','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/townhouse_2.png\", \"size\": [122, 73]}'),('sorl-thumbnail||image||2f4fbc5445749a236643efd6a916adae','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c5/46/c546e2a50b9f76d36b1c5df28e423fbb.jpg\", \"size\": [120, 72]}'),('sorl-thumbnail||thumbnails||f1d7cebdcf07e36330fdb9e06806e059','[\"f54d7553c0f442911f9f068284a32521\", \"2f4fbc5445749a236643efd6a916adae\"]'),('sorl-thumbnail||image||e120ddb87083f0726bbf3bab1fde3646','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/townhouse_1.png\", \"size\": [122, 73]}'),('sorl-thumbnail||image||f7a40c448574ab9b0537ff36928b8fe4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5f/77/5f77cdfb153cbaf75e7de54dc7e43fb8.jpg\", \"size\": [120, 72]}'),('sorl-thumbnail||thumbnails||e120ddb87083f0726bbf3bab1fde3646','[\"4f253ba06c10662be8286996f418a9a5\", \"f7a40c448574ab9b0537ff36928b8fe4\"]'),('sorl-thumbnail||image||80a33a71d8367a61ab797fde3280df4a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/townhouse.png\", \"size\": [122, 73]}'),('sorl-thumbnail||image||1859088e66dd583b983e50402ab0040d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/19/ae/19aeda94faa65009067463a4dd25d656.jpg\", \"size\": [120, 72]}'),('sorl-thumbnail||thumbnails||80a33a71d8367a61ab797fde3280df4a','[\"6b22225da77df7bbdaf7910aea41766b\", \"1859088e66dd583b983e50402ab0040d\"]'),('sorl-thumbnail||image||5610fa0dc4941470e643217c1e7d4424','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/townhouse2_1.png\", \"size\": [142, 81]}'),('sorl-thumbnail||image||59bff7dc22737bb2bfd13454337fafab','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e9/56/e956616c7dd10509d894f386010476aa.jpg\", \"size\": [119, 68]}'),('sorl-thumbnail||thumbnails||5610fa0dc4941470e643217c1e7d4424','[\"59bff7dc22737bb2bfd13454337fafab\", \"27936ddd3bc2e7df58e01a3f994a54cf\"]'),('sorl-thumbnail||image||52564e1bcec2aca4bf42dea3b6792079','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/townhouse2.png\", \"size\": [142, 81]}'),('sorl-thumbnail||image||fe8cdb2dbe5334f0c00640bbd968ddca','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ad/45/ad450069982f202aed3c1a8e40940202.jpg\", \"size\": [119, 68]}'),('sorl-thumbnail||thumbnails||52564e1bcec2aca4bf42dea3b6792079','[\"fe8cdb2dbe5334f0c00640bbd968ddca\", \"d458355df3c3de102c0a492caf29187b\"]'),('sorl-thumbnail||image||813bf1c3db237c29a8ef6d6aae779c40','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e1/55/e1554f0243cf8ba4df492d55a9ea45e9.png\", \"size\": [119, 63]}'),('sorl-thumbnail||image||7e9405e7db622ed255f223d3cc5fb870','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a1/c8/a1c8fe335406c5fcee35d4e1aed29b73.png\", \"size\": [119, 75]}'),('sorl-thumbnail||image||a4a25cce60b167d8ac1319dd70cfdd5c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/57/5e/575e2c0d9edc822fa437ade4076c14f1.png\", \"size\": [120, 74]}'),('sorl-thumbnail||image||f54d7553c0f442911f9f068284a32521','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/72/9c/729cb1c8edc96a431f87e8189257b09d.png\", \"size\": [120, 72]}'),('sorl-thumbnail||image||4f253ba06c10662be8286996f418a9a5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/9b/32/9b329fa64f2c079d86b2f5a7da57dd22.png\", \"size\": [120, 72]}'),('sorl-thumbnail||image||6b22225da77df7bbdaf7910aea41766b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/aa/4d/aa4d3291adc816b4dc3a97f312480fda.png\", \"size\": [120, 72]}'),('sorl-thumbnail||image||27936ddd3bc2e7df58e01a3f994a54cf','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f4/7f/f47fd45e28ca3e00d378b681b657d4c9.png\", \"size\": [119, 68]}'),('sorl-thumbnail||image||d458355df3c3de102c0a492caf29187b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/db/36/db361a724292338220758033e3e86d44.png\", \"size\": [119, 68]}'),('sorl-thumbnail||image||9b01337cb8fa9e5ab1c23aee60365851','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example1.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||5a0f8e10e030abf86ed692703a9918ab','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6c/62/6c628d44a2a6a118c049ff53ef649c3a.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||9b01337cb8fa9e5ab1c23aee60365851','[\"5a0f8e10e030abf86ed692703a9918ab\"]'),('sorl-thumbnail||image||13b64915b6d693e916ab6c619673a62f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example2.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||e34e27b3a2fc424e9697b68dd32d7e3e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5c/98/5c98535fe86ad1ba3573e1ed578afcba.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||13b64915b6d693e916ab6c619673a62f','[\"e34e27b3a2fc424e9697b68dd32d7e3e\"]'),('sorl-thumbnail||image||718025d0adb7ebf37371e89710ba06ce','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example3.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||051431f3ceddc3521392fb3d1d5c9305','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/22/19/2219b16cf35e79497ebbbd9dd9cec31f.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||718025d0adb7ebf37371e89710ba06ce','[\"051431f3ceddc3521392fb3d1d5c9305\"]'),('sorl-thumbnail||image||1ac155ee5518284a5eba1225b94d6475','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example4.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||ebde8c3b6339ba34e08c5dd6efe9970d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ea/32/ea3267fecdd6df7212d2139d05a8ed80.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||1ac155ee5518284a5eba1225b94d6475','[\"ebde8c3b6339ba34e08c5dd6efe9970d\"]'),('sorl-thumbnail||image||525bfa273b8b9c28f3f86ee78a6e27a6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example5.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||013c3f36c64bfbe03b035e48f5e63927','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/8a/fb/8afb0d890efcd7b7110133dc371affd2.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||525bfa273b8b9c28f3f86ee78a6e27a6','[\"013c3f36c64bfbe03b035e48f5e63927\"]'),('sorl-thumbnail||image||8399b0e05d22d5c79529f3862d7aba90','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example6.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||2bd84ed462cac646e7ea6818b5da3ae2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/34/f3/34f3bc6b6787dc3ea1f5bf7c84cf0dfd.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||8399b0e05d22d5c79529f3862d7aba90','[\"2bd84ed462cac646e7ea6818b5da3ae2\"]'),('sorl-thumbnail||image||a8ad636f9e4fc93c50217123e8d81b43','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/roomphoto/room-example7.jpg\", \"size\": [600, 399]}'),('sorl-thumbnail||image||02e3a4533be8727c00c4da7cab3974c5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c8/71/c871675bdaf8bf0eace73ddb4e9a97eb.jpg\", \"size\": [74, 74]}'),('sorl-thumbnail||thumbnails||a8ad636f9e4fc93c50217123e8d81b43','[\"02e3a4533be8727c00c4da7cab3974c5\"]'),('sorl-thumbnail||image||2beb1626ff7b9872ff2597862ec5f937','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/8/townhouse2.png\", \"size\": [142, 81]}'),('sorl-thumbnail||image||ebf4646b94c04c9f6c2d8fb4e52cd64d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/fe/ab/feab67385e1b9725b642b1c5d114222e.png\", \"size\": [119, 68]}'),('sorl-thumbnail||thumbnails||2beb1626ff7b9872ff2597862ec5f937','[\"ebf4646b94c04c9f6c2d8fb4e52cd64d\"]'),('sorl-thumbnail||image||8ab69991e86843997ee00cc89e3cc5af','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/hotel/6/townhouse.png\", \"size\": [122, 73]}'),('sorl-thumbnail||image||3179f4d05fe67ece99707130a030f37f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6f/77/6f7735f4ab423854899960fde6b0811c.png\", \"size\": [120, 72]}'),('sorl-thumbnail||thumbnails||8ab69991e86843997ee00cc89e3cc5af','[\"3179f4d05fe67ece99707130a030f37f\"]');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-05 19:04:43
