# -*- coding: utf-8 -*-

from fabric.api import *

env.hosts = ['root@62.76.184.19']
env.passwords = {'root@62.76.184.19':'6421S1309c'}
project_repository = 'https://grigl@bitbucket.org/grigl/aurora.git'
project_name = 'aurora'
mysql_password = 'jQ93Hb9'

def cmp(commit_text):
    local('git add .')
    local('git commit -m "%s"' % commit_text)
    local('git push origin master')

def first_deploy():
    with cd('/home/user/'):
        run('git clone %s' % project_repository)

    with cd('/home/user/%s' % project_name):
        # файлы проекта
        run('wget http://octoberweb.ru/static/fabric/django_wsgi.py')

        run('touch uwsgi.xml')
        run("echo '<uwsgi>' >> uwsgi.xml")
        run("echo '    <pythonpath>/home/user/%s</pythonpath>' >> uwsgi.xml" % project_name)
        run("echo '    <pythonpath>/home/user</pythonpath>' >> uwsgi.xml")
        run("echo '    <app mountpoint=\042/\042>' >> uwsgi.xml")
        run("echo '        <script>django_wsgi</script>' >> uwsgi.xml")
        run("echo '    </app>' >> uwsgi.xml")
        run("echo '    <touch-reload>/home/user/%s/reload.txt</touch-reload>' >> uwsgi.xml" % project_name)
        run("echo '</uwsgi>' >> uwsgi.xml")

        run('touch reload.txt')

        run('touch config/dbpass.py')
        dbpass = "DATABASE_PASSWORD = '%s'" % mysql_password
        run('echo "%s" >> config/dbpass.py' % dbpass)

        # бд
        run("mysql -u'root' -p'%s' -e 'create database %s character set utf8;'" % (mysql_password, project_name))
        run("python manage.py syncdb")
        run("python manage.py migrate")

def commit():
    """
    Коммит изменений.
    """
    with settings(warn_only=True):
        local('git status')
        prompt('Press <Enter> to continue or <Ctrl+C> to cancel.')  # тут можно прервать коммит, 
            # если в него попали ненужные файлы или наоборот
        local('git add .')
        local('git commit')  # тут вылазит консольный редактор и можно ввести комментарий
        local('git push origin master')

def deploy():
    with cd('/home/user/%s' % project_name):
        run('git fetch --all')
        run('git reset --hard origin/master')
        run("python manage.py migrate --ignore-ghost-migrations")
        run('touch reload.txt')

# def temp():
#   with cd('/home/user/%s' % project_name):
#       run("python manage.py migrate --ignore-ghost-migrations")

def load_db():
    db_name = prompt('enter db name:')

    with cd('/home/user/%s' % project_name):
        run("mysql -u'root' -p'%s' %s < %s" % (mysql_password, project_name, db_name))  

def cherokee_admin():
    run('cherokee-admin -u -b')

def touch_reload():
    with cd('/home/user/%s' % project_name):
        run('touch reload.txt')

def restart():
    with cd('/home/user/%s' % project_name):
        run('service cherokee restart')