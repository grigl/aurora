# -*- coding: utf-8 -*-
import os, json
from django import http
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView, View
from pytils.translit import translify
from django.views.decorators.csrf import csrf_exempt
try:
    from PIL import Image
except ImportError:
    import Image
import md5
import datetime
from django.contrib.auth.decorators import login_required
from apps.utils.utils import crop_image
from django.db.models import get_model 

from apps.rooms.models import Season, Price, PriceType, RoomType
from time import strptime, strftime, mktime

def handle_uploaded_file(f, filename, folder):
    name, ext = os.path.splitext(translify(filename).replace(' ', '_'))
    hashed_name=md5.md5(name+datetime.datetime.now().strftime("%Y%m%d%H%M%S")).hexdigest()
    path_name = settings.MEDIA_ROOT + '/uploads/' + folder + hashed_name + ext
    destination = open(path_name, 'wb+')

    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    return '/media/uploads/'+ folder + hashed_name + ext

@csrf_exempt
def upload_img(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'images/')

            #Resizing
            size = 600, 600
            im = Image.open(settings.ROOT_PATH + url)
            imageSize=im.size
            if (imageSize[0] > size[0]) or  (imageSize[1] > size[1]):
                im.thumbnail(size, Image.ANTIALIAS)
                im.save(settings.ROOT_PATH + url, "JPEG", quality = 100)
            return http.HttpResponse('{"filelink":"%s"}'%url)

        else:
            return http.HttpResponse('error')
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')

@csrf_exempt
def upload_file(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'files/')
            url = '{"filelink":"%s","filename":"%s"}' % (url, request.FILES['file'].name)
            return http.HttpResponse(url)
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')

@login_required()
@csrf_exempt
def crop_image_view(request, app_name, model_name, id):
    model = get_model(app_name, model_name)
    output_size = model.crop_size
    if request.method != "POST":
        try:
            image = model.objects.get(pk=id).image
            return direct_to_template(request, 'admin/crop_image.html', locals())
        except model.DoesNotExist:
            raise http.Http404('Object not found')
    else:
        original_img = model.objects.get(pk=id)
        crop_image(request.POST, original_img, output_size)

    next = request.path.replace('crop/', '')
    return http.HttpResponseRedirect(next)


class RoomsPricesView(TemplateView):
    template_name = 'admin/rooms_prices.html'

    def get_season(self, request):
        if request.GET.get('season'):
            try:
                season = Season.objects.get(id=request.GET.get('season'))
            except:
                season = Season.get_current()
        else:
            season = Season.get_current()

        return season

    def get_context_data(self, **kwargs):
        context = super(RoomsPricesView, self).get_context_data()

        season = self.get_season(self.request)
        archieved_seasons = Season.objects.filter(date_to__lt=datetime.date.today())

        room_price_list = season.get_room_price_list()

        context['room_price_list'] = room_price_list
        context['season'] = season
        context['archieved_seasons'] = archieved_seasons
        return context


class ChangeRoomPriceView(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ChangeRoomPriceView, self).dispatch(*args, **kwargs)

    def post(self, request, **kwargs):
        price_id = request.POST.get('price_id')
        new_price_val = request.POST.get('price_val')

        if price_id:
            price = Price.objects.get(pk=price_id)

        if price and price.price != new_price_val:
            price.price = new_price_val
            price.save()

        return HttpResponse('success')


class CreatePricesView(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(CreatePricesView, self).dispatch(*args, **kwargs)

    def post(self, request, **kwargs):
        title = request.POST.get('title')
        description = request.POST.get('description')
        season = request.POST.get('season')

        if title and title != '':
            price_type = PriceType.objects.create(title=title, description=description)
            price_type.save()

            season = Season.objects.get(pk=season)
            room_types = RoomType.objects.all()

            new_prices = []
            for room_type in room_types:
                price = Price.objects.create(season=season, room_type=room_type, price_type=price_type)
                price.save()
                new_prices.append({'price_id': price.id, 'room_type_id': price.room_type.id})

            return HttpResponse(json.dumps({'success': '1'}))
        else:
            return HttpResponse(json.dumps({'success': '0'}))


class SavePriceTypeView(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(SavePriceTypeView, self).dispatch(*args, **kwargs)

    def post(self, request, **kwargs):
        title = request.POST.get('title')
        description = request.POST.get('description')
        price_type_id = request.POST.get('id')

        try: 
            price_type = PriceType.objects.get(pk=price_type_id)
        except:
            price_type = None

        if price_type:
            price_type.description = description
            price_type.title = title
            price_type.save()

            return HttpResponse(json.dumps({'success': '1'}))
        else:
            return HttpResponse(json.dumps({'success': '0'}))


class SaveSeasonView(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(SaveSeasonView, self).dispatch(*args, **kwargs)

    def post(self, request, **kwargs):
        title = request.POST.get('title')
        label = request.POST.get('label')
        date_to = request.POST.get('date_to')
        date_from = request.POST.get('date_from')
        season_id = request.POST.get('id')

        try: 
            season = Season.objects.get(pk=season_id)
        except:
            season = None

        if season:
            season.title = title
            season.special_offer_label = label
            season.date_to = datetime.datetime.fromtimestamp(mktime(strptime(date_to, '%d/%m/%Y')))
            season.date_from = datetime.datetime.fromtimestamp(mktime(strptime(date_from, '%d/%m/%Y')))
            season.save()

            return HttpResponse(json.dumps({'success': '1'}))
        else:
            return HttpResponse(json.dumps({'success': '0'}))


class CreateSeasonView(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(CreateSeasonView, self).dispatch(*args, **kwargs)

    def post(self, request, **kwargs):
        title = request.POST.get('title')
        label = request.POST.get('label')
        date_to = request.POST.get('date-to')
        date_from = request.POST.get('date-from')
        
        season = Season.objects.create(
            title = title,
            special_offer_label = label,
            date_to = datetime.datetime.fromtimestamp(mktime(strptime(date_to, '%d/%m/%Y'))),
            date_from = datetime.datetime.fromtimestamp(mktime(strptime(date_from, '%d/%m/%Y')))
        )

        destination_url = '/admin/rooms_prices/?season=%s' % season.id
        return HttpResponseRedirect(destination_url)

        