# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.http import HttpResponseRedirect

#from apps.app.urls import urlpatterns as app_url

from views import *
from apps.rooms.views import *
from apps.services.views import *
from apps.press.views import *
from apps.reviews.views import *

urlpatterns = patterns('',
    url(r'^$',index, name='index'),
    url(r'^faq/', include('apps.faq.urls')),
    url(r'^cart/', include('apps.orders.urls')),
    url(r'^cabinet/', include('apps.inheritanceUser.urls')),

    url(r'^full_map/$', FullMapView.as_view(), name="full_map_url"),

    # rooms urls 
    url(r'^rooms/$', HotelListView.as_view(), name="hotel_list_url"),
    url(r'^rooms/hotels/(?P<slug>[\w\d_/-]+)/$', HotelDetailView.as_view(), name="hotel_detail_url"),
    url(r'^rooms/townhouses/(?P<slug>[\w\d_/-]+)/$', TownhouseDetailView.as_view(), name="townhouse_detail_url"),

    # services urls
    url(r'^services/$', ServiceListView.as_view(), name="service_list_url"),
    url(r'^services/(?P<slug>[\w\d_/-]+)/$', ServiceDetailView.as_view(), name="service_detail_url"),

    # prices urls
    url(r'^prices/$', lambda x: HttpResponseRedirect('/prices/rooms/'), name="all_price_list_url"),
    url(r'^prices/rooms/$', RoomsPriceListView.as_view(), name="rooms_price_list_url"),
    url(r'^prices/services/$', ServicesPriceListView.as_view(), name="services_price_list_url"),
    url(r'^prices/get_season_table/$', SeasonTableView.as_view(), name="get_season_table_url"),
    url(r'^prices/order/$', OrderView.as_view(), name="order_url"),

    # about views
    url(r'^info/$', lambda x: HttpResponseRedirect('/info/about/'), name="all_price_list_url"),
    url(r'^info/about/$', AboutView.as_view(), name="about_url"),
    url(r'^info/press/$', PressListView.as_view(), name="press_list_url"),
    url(r'^info/press/(?P<pk>\d+)/$', PressItemView.as_view(), name="press_item_url"),

    # restaurants views
    url(r'^restaurants/$', RestaurantsView.as_view(), name="restaurants_url"),

    # specials views 
    url(r'^for_corporate_clients/$', lambda x: HttpResponseRedirect('/for_corporate_clients/places/'), name="corpo_url"),
    url(r'^for_corporate_clients/places/$', CorpoPlacesView.as_view(), name="corpo_places_url"),
    url(r'^for_corporate_clients/teambuilding/$', TeambuildingView.as_view(), name="teambuilding_url"),
    url(r'^weddings_and_celebrations/$', WeddingsView.as_view(), name="weddings_url"),

    # reviews urls
    url(r'^reviews/$', ReviewListView.as_view(), name="review_list_url"),
    url(r'^reviews/review_form/$', ReviewFormView.as_view(), name="review_form_url"),
)
#url(r'^captcha/', include('captcha.urls')),

#urlpatterns += #app_url


