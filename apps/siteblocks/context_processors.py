# -*- coding: utf-8 -*-
from apps.siteblocks.models import Settings, TextBlock
from settings import SITE_NAME

def settings(request):
    try:
        summer_or_winter = Settings.objects.get(name='summer_or_winter').value
    except:
        summer_or_winter = None

    try:
        contacts = Settings.objects.get(name='contacts_block').value
    except Settings.DoesNotExist:
        contacts = False

    return {
        'contacts': contacts,
        'site_name': SITE_NAME,
        'summer_or_winter': summer_or_winter
    }


def textblocks(request):
    try:
        text_about_aurora = TextBlock.objects.get(name='text_about_aurora').value
    except:
        text_about_aurora = None

    return {
        'text_about_aurora': text_about_aurora,
    }

