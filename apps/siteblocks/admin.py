# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.siteblocks.models import SiteMenu, Settings, TextBlock, Gallery, GalleryImage
from apps.utils.widgets import RedactorMini
from sorl.thumbnail.admin import AdminImageMixin
from mptt.admin import MPTTModelAdmin
from apps.utils.widgets import Redactor



class SiteMenuAdmin(AdminImageMixin, MPTTModelAdmin):
    list_display = ('title', 'url', 'menu_type','order', 'is_published',)
    list_display_links = ('title', 'url',)
    list_editable = ('order', 'is_published',)

admin.site.register(SiteMenu, SiteMenuAdmin)


class GalleryImageInline(admin.TabularInline):
    model = GalleryImage
    extra = 0 


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_published',)
    list_editable = ('is_published',)
    inlines = [GalleryImageInline]

admin.site.register(Gallery, GalleryAdmin)

#--Виджеты jquery Редактора
class SettingsAdminForm(forms.ModelForm):
    class Meta:
        model = Settings

    def __init__(self, *args, **kwargs):
        super(SettingsAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
        if instance:
            if instance.type == u'input':
                self.fields['value'].widget = forms.TextInput()
            elif instance.type == u'textarea':
                self.fields['value'].widget = forms.Textarea()
            elif instance.type == u'redactor':
                self.fields['value'].widget = Redactor(attrs={'cols': 100, 'rows': 10},)

#--Виджеты jquery Редактора

class SettingsAdmin(admin.ModelAdmin):
    list_display = ('title','name','value',)
    form = SettingsAdminForm
admin.site.register(Settings, SettingsAdmin)


#--Виджеты jquery Редактора
class TextBlockAdminForm(forms.ModelForm):
    class Meta:
        model = TextBlock

    def __init__(self, *args, **kwargs):
        super(TextBlockAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
        if instance:
            if instance.type == u'input':
                self.fields['value'].widget = forms.TextInput()
            elif instance.type == u'textarea':
                self.fields['value'].widget = forms.Textarea()
            elif instance.type == u'redactor':
                self.fields['value'].widget = Redactor(attrs={'cols': 100, 'rows': 10},)

#--Виджеты jquery Редактора

class TextBlockAdmin(admin.ModelAdmin):
    list_display = ('title','name','value',)
    form = TextBlockAdminForm
admin.site.register(TextBlock, TextBlockAdmin)
