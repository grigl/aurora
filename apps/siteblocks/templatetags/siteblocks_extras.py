# -*- coding: utf-8 -*-
from apps.siteblocks.models import SiteMenu, Settings, Gallery, TextBlock
from apps.services.models import Service
from apps.press.models import SpecialOffer
from django import template
from django.utils.safestring import mark_safe

register = template.Library()

def get_active_menu(url, site_menu):
    
    request_path = url
    
    for i, menu_item in enumerate(site_menu):
        setattr(site_menu[i], 'is_active', False)
        #if request_path.startswith(menu_item.path):
            #site_menu[i].is_active = True


    for i, menu_item in enumerate(site_menu):
        setattr(site_menu[i], 'is_active', False)
        if not menu_item.parent:
            for menu_subitem in site_menu:
                # highlight parent menu item
                if menu_subitem.parent == menu_item and menu_subitem.is_active:
                    site_menu[i].is_active = True
        elif menu_item.is_active:
            for menu_subitem in site_menu:
                # remove redundant sibling highlight
                if menu_subitem.parent \
                     and menu_subitem.parent.path == menu_item.parent.path \
                     and menu_subitem.path.startswith(menu_item.path) \
                     and len(menu_subitem.path) > len(menu_item.path) \
                     and menu_subitem.is_active:
                        site_menu[i].is_active = False
    return site_menu

@register.inclusion_tag("siteblocks/block_menu.html")
def main_menu(url):
    url = url.split('/')

    if url[1]:
        current = u'/%s/' % url[1]
    else:
        current = u'/'
    menu = SiteMenu.objects.filter(menu_type='main').order_by('order')
    root = '/'
    return {'menu': menu, 'current': current, 'root': root}


@register.inclusion_tag("siteblocks/block_menu.html")
def about_menu(url):
    current = url
    menu = SiteMenu.objects.filter(menu_type='about')
    root = '/info/'
    return {'menu': menu, 'current': current, 'root': root}


@register.inclusion_tag("siteblocks/block_menu.html")
def price_menu(url):
    current = url
    menu = SiteMenu.objects.filter(menu_type='prices')
    root = '/prices/'
    return {'menu': menu, 'current': current, 'root': root}

@register.inclusion_tag("siteblocks/block_menu.html")
def corpo_menu(url):
    current = url
    menu = SiteMenu.objects.filter(menu_type='corpo')
    root = 'for_corporate_clients/'
    return {'menu': menu, 'current': current, 'root': root}


@register.inclusion_tag("siteblocks/block_setting.html")
def block_static(name):
    try:
        setting = Settings.objects.get(name = name)
    except Settings.DoesNotExist:
        setting = False
    return {'block': block,}


@register.inclusion_tag("siteblocks/sidebar_two_addresses.html")
def get_sidebar_two_addresses(sticker=False):
    try:
        office_address = TextBlock.objects.get(name='office_address').value
    except:
        office_address = False

    try:
        resort_address = TextBlock.objects.get(name='resort_address').value
    except:
        resort_address = False

    return {'sticker':sticker, 'office_address': office_address, 'resort_address': resort_address}


@register.inclusion_tag("siteblocks/sidebar_review.html")
def get_sidebar_review(sticker=False):
    try:
        office_address = TextBlock.objects.get(name='office_address').value
    except:
        office_address = False
    
    return {'sticker':sticker, 'office_address': office_address}


@register.inclusion_tag("siteblocks/sidebar_order.html")
def get_sidebar_order(sticker=False):
    try:
        office_address = TextBlock.objects.get(name='office_address').value
    except:
        office_address = False
    
    return {'sticker':sticker, 'office_address': office_address}


@register.inclusion_tag("siteblocks/get_hotel_page_service_label.html")
def get_hotel_page_service_label(service_slug):
    try:
        service = Service.objects.get(slug=service_slug)
    except:
        service = None

    return {'service': service,}


@register.inclusion_tag("siteblocks/get_index_map_label.html")
def get_index_map_label(service_slug):
    try:
        service = Service.objects.get(slug=service_slug)
    except:
        service = None

    return {'service': service,}


@register.inclusion_tag("siteblocks/get_period_string.html")
def get_period_string(date_from, date_to):
    return {'date_from': date_from, 'date_to': date_to}
   

@register.inclusion_tag("siteblocks/get_special_offers_slider.html")
def get_special_offers_slider(waved):
    special_offers = SpecialOffer.objects.published().filter(show_in_slider=True).order_by('-date_add')

    return {'special_offers': special_offers, 'waved': waved }


@register.filter()
def spanlines(value):
    result = '<span>' + value.replace('\n', '</span><span>') + '</span>'

    return mark_safe(result)

    
@register.filter()
def formatprice(value):
    if value == '' or value == None:
        result = ''
    else:
        result = str("{:,}".format(value)).split('.')[0].replace(',','&thinsp;')

    return mark_safe(result)


@register.filter()
def get_classname(inst):
    return inst.__class__.__name__


