# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
import datetime
import os
from pytils.translit import translify
from django.db.models.signals import post_save
from apps.utils.managers import PublishedManager
from mptt.models import MPTTModel, TreeForeignKey, TreeManager

from sorl.thumbnail import ImageField

def image_path(self, instance, filename):
    filename = translify(filename).replace(' ', '_')
    return os.path.join('uploads', 'images/menu', filename)

def gallery_image_path(instance, filename):
    filename = translify(filename).replace(' ', '_')
    galleryname = translify(instance.gallery.title).replace(' ', '_')
    return os.path.join('uploads', 'images/galleries', galleryname, filename)

class SiteMenu(MPTTModel):
    MENU_TYPE_CHOISES = (
        ('main','главное меню'),
        ('prices','меню страницы цен'),
        ('about', 'меню страницы "о курорте"'),
        ('corpo', 'меню страницы "корпоративным клиентам"'),
    )

    title = models.CharField(
        max_length = 150, 
        verbose_name = u'Название'
    )
    image = models.ImageField(
        verbose_name=u'Иконка',
        upload_to = image_path, 
        blank = True,
        null = True,
    )
    parent = TreeForeignKey(
        'self',
        verbose_name = u'Родительский раздел',
        related_name = 'children',
        blank = True,
        null = True,
    )
    url = models.CharField(
        verbose_name = u'url', 
        max_length = 150, 
    )
    order = models.IntegerField(
        verbose_name = u'Порядок сортировки',
        default = 10,
        help_text = u'чем больше число, тем выше располагается элемент'
    )
    is_published = models.BooleanField(
        verbose_name=u'Опубликовано',
        default=True, 
    )
    menu_type = models.CharField(verbose_name=u'тип меню', max_length=50, choices=MENU_TYPE_CHOISES)

    objects = TreeManager()

    def save(self, *args, **kwargs):
        self.title = self.title.strip()
        self.url = self.url.strip()
        super(SiteMenu, self).save()

    class Meta:
        verbose_name =_(u'menu_item')
        verbose_name_plural =_(u'menu_items')
        ordering = ['-order']

    class MPTTMeta:
        order_insertion_by = ['order']

    def __unicode__(self):
        return self.title

type_choices = (
    (u'input',u'input'),
    (u'textarea',u'textarea'),
    (u'redactor',u'redactor'),
)

class Settings(models.Model):
    title = models.CharField(
        verbose_name = u'Название',
        max_length = 150,
    )
    name = models.CharField( 
        verbose_name = u'Служебное имя',
        max_length = 250,
    )
    value = models.TextField(
        verbose_name = u'Значение'
    )
    type = models.CharField(
        max_length=20,
        verbose_name=u'Тип значения',
        choices=type_choices
    )
    class Meta:
        verbose_name =_(u'site_setting')
        verbose_name_plural =_(u'site_settings')

    def __unicode__(self):
        return u'%s' % self.name



class TextBlock(models.Model):
    title = models.CharField(
        verbose_name = u'Название',
        max_length = 150,
    )
    name = models.CharField( 
        verbose_name = u'Служебное имя',
        max_length = 250,
    )
    value = models.TextField(
        verbose_name = u'Значение'
    )
    type = models.CharField(
        max_length=20,
        verbose_name=u'Тип значения',
        choices=type_choices
    )
    class Meta:
        verbose_name = u'Текстовый блок'
        verbose_name_plural = u'Текстовые блоки'

    def __unicode__(self):
        return u'%s' % self.name


class Gallery(models.Model):
    gallery_type_choices = (
        (u'about',u'галлерея страницы "О курорте"'),
        (u'corpo_places',u'галлерея страницы "площадки"'),
        (u'teambuilding',u'галлерея страницы "тимбилдинг"'),
        (u'weddings',u'галлерея страницы "свадьбы и торжества"'),
    )

    title = models.CharField(verbose_name=u'название', max_length=150)
    is_published = models.BooleanField(verbose_name=u'опубликовано', default=True)
    gallery_type = models.CharField(verbose_name=u'тип галлереи', max_length=150, choices=gallery_type_choices)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'фотогаллерея'
        verbose_name_plural = u'фотогаллереи'

    def __unicode__(self):
        return self.title


class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery, verbose_name=u'галлерея')
    image = ImageField(verbose_name=u'картинка', upload_to=gallery_image_path)

    class Meta:
        verbose_name = u'фотография'
        verbose_name_plural = u'фотографии'

    def __unicode__(self):
        return self.image.name
    
    

