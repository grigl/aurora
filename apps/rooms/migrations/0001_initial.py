# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Photo'
        db.create_table('rooms_photo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('rooms', ['Photo'])

        # Adding model 'Panorama'
        db.create_table('rooms_panorama', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('panorama', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('rooms', ['Panorama'])

        # Adding model 'RoomConfig'
        db.create_table('rooms_roomconfig', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('rooms', ['RoomConfig'])

        # Adding model 'Hotel'
        db.create_table('rooms_hotel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('hotel_type', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('room_config', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.RoomConfig'])),
            ('short_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('rooms', ['Hotel'])

        # Adding model 'RoomType'
        db.create_table('rooms_roomtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('room_config', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.RoomConfig'])),
        ))
        db.send_create_signal('rooms', ['RoomType'])

        # Adding model 'Room'
        db.create_table('rooms_room', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('room_config', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.RoomType'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('rooms', ['Room'])

        # Adding M2M table for field photos on 'Room'
        db.create_table('rooms_room_photos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('room', models.ForeignKey(orm['rooms.room'], null=False)),
            ('photo', models.ForeignKey(orm['rooms.photo'], null=False))
        ))
        db.create_unique('rooms_room_photos', ['room_id', 'photo_id'])

        # Adding M2M table for field panoramas on 'Room'
        db.create_table('rooms_room_panoramas', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('room', models.ForeignKey(orm['rooms.room'], null=False)),
            ('panorama', models.ForeignKey(orm['rooms.panorama'], null=False))
        ))
        db.create_unique('rooms_room_panoramas', ['room_id', 'panorama_id'])

        # Adding model 'PriceType'
        db.create_table('rooms_pricetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('rooms', ['PriceType'])

        # Adding model 'Season'
        db.create_table('rooms_season', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('date_from', self.gf('django.db.models.fields.DateField')()),
            ('date_to', self.gf('django.db.models.fields.DateField')()),
            ('special_offer_label', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('rooms', ['Season'])

        # Adding model 'Price'
        db.create_table('rooms_price', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.RoomType'])),
            ('price_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.PriceType'])),
            ('season', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.Season'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal('rooms', ['Price'])


    def backwards(self, orm):
        # Deleting model 'Photo'
        db.delete_table('rooms_photo')

        # Deleting model 'Panorama'
        db.delete_table('rooms_panorama')

        # Deleting model 'RoomConfig'
        db.delete_table('rooms_roomconfig')

        # Deleting model 'Hotel'
        db.delete_table('rooms_hotel')

        # Deleting model 'RoomType'
        db.delete_table('rooms_roomtype')

        # Deleting model 'Room'
        db.delete_table('rooms_room')

        # Removing M2M table for field photos on 'Room'
        db.delete_table('rooms_room_photos')

        # Removing M2M table for field panoramas on 'Room'
        db.delete_table('rooms_room_panoramas')

        # Deleting model 'PriceType'
        db.delete_table('rooms_pricetype')

        # Deleting model 'Season'
        db.delete_table('rooms_season')

        # Deleting model 'Price'
        db.delete_table('rooms_price')


    models = {
        'rooms.hotel': {
            'Meta': {'ordering': "['title']", 'object_name': 'Hotel'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel_type': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.panorama': {
            'Meta': {'object_name': 'Panorama'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panorama': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.photo': {
            'Meta': {'object_name': 'Photo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.price': {
            'Meta': {'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'price_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.PriceType']"}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Season']"})
        },
        'rooms.pricetype': {
            'Meta': {'object_name': 'PriceType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panoramas': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rooms.Panorama']", 'symmetrical': 'False'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rooms.Photo']", 'symmetrical': 'False'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomconfig': {
            'Meta': {'object_name': 'RoomConfig'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.season': {
            'Meta': {'object_name': 'Season'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'special_offer_label': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['rooms']