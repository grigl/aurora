# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Panorama'
        db.delete_table('rooms_panorama')

        # Deleting model 'Photo'
        db.delete_table('rooms_photo')

        # Adding model 'RoomPhoto'
        db.create_table('rooms_roomphoto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.Room'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('rooms', ['RoomPhoto'])

        # Adding model 'RoomPanorama'
        db.create_table('rooms_roompanorama', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.Room'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('panorama', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('rooms', ['RoomPanorama'])

        # Removing M2M table for field photos on 'Room'
        db.delete_table('rooms_room_photos')

        # Removing M2M table for field panoramas on 'Room'
        db.delete_table('rooms_room_panoramas')


    def backwards(self, orm):
        # Adding model 'Panorama'
        db.create_table('rooms_panorama', (
            ('panorama', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
        ))
        db.send_create_signal('rooms', ['Panorama'])

        # Adding model 'Photo'
        db.create_table('rooms_photo', (
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
        ))
        db.send_create_signal('rooms', ['Photo'])

        # Deleting model 'RoomPhoto'
        db.delete_table('rooms_roomphoto')

        # Deleting model 'RoomPanorama'
        db.delete_table('rooms_roompanorama')

        # Adding M2M table for field photos on 'Room'
        db.create_table('rooms_room_photos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('room', models.ForeignKey(orm['rooms.room'], null=False)),
            ('photo', models.ForeignKey(orm['rooms.photo'], null=False))
        ))
        db.create_unique('rooms_room_photos', ['room_id', 'photo_id'])

        # Adding M2M table for field panoramas on 'Room'
        db.create_table('rooms_room_panoramas', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('room', models.ForeignKey(orm['rooms.room'], null=False)),
            ('panorama', models.ForeignKey(orm['rooms.panorama'], null=False))
        ))
        db.create_unique('rooms_room_panoramas', ['room_id', 'panorama_id'])


    models = {
        'rooms.hotel': {
            'Meta': {'ordering': "['title']", 'object_name': 'Hotel'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.HotelType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.hoteltype': {
            'Meta': {'object_name': 'HotelType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.price': {
            'Meta': {'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'price_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.PriceType']"}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Season']"})
        },
        'rooms.pricetype': {
            'Meta': {'object_name': 'PriceType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Hotel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomconfig': {
            'Meta': {'object_name': 'RoomConfig'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roompanorama': {
            'Meta': {'object_name': 'RoomPanorama'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panorama': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Room']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.roomphoto': {
            'Meta': {'object_name': 'RoomPhoto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Room']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.season': {
            'Meta': {'object_name': 'Season'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'special_offer_label': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['rooms']