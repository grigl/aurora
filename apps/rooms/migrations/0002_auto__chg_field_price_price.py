# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Price.price'
        db.alter_column('rooms_price', 'price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2))

    def backwards(self, orm):

        # Changing field 'Price.price'
        db.alter_column('rooms_price', 'price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2))

    models = {
        'rooms.hotel': {
            'Meta': {'ordering': "['title']", 'object_name': 'Hotel'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel_type': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.panorama': {
            'Meta': {'object_name': 'Panorama'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panorama': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.photo': {
            'Meta': {'object_name': 'Photo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.price': {
            'Meta': {'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'price_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.PriceType']"}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Season']"})
        },
        'rooms.pricetype': {
            'Meta': {'object_name': 'PriceType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panoramas': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rooms.Panorama']", 'symmetrical': 'False'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rooms.Photo']", 'symmetrical': 'False'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomconfig': {
            'Meta': {'object_name': 'RoomConfig'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.season': {
            'Meta': {'object_name': 'Season'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'special_offer_label': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['rooms']