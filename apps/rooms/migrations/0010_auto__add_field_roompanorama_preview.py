# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RoomPanorama.preview'
        db.add_column('rooms_roompanorama', 'preview',
                      self.gf('django.db.models.fields.files.FileField')(default=1, max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'RoomPanorama.preview'
        db.delete_column('rooms_roompanorama', 'preview')


    models = {
        'rooms.hotel': {
            'Meta': {'ordering': "['title']", 'object_name': 'Hotel'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.HotelType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.hoteltype': {
            'Meta': {'object_name': 'HotelType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.price': {
            'Meta': {'ordering': "['price_type']", 'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'price_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.PriceType']"}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomType']"}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Season']"})
        },
        'rooms.pricetype': {
            'Meta': {'object_name': 'PriceType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Hotel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roomconfig': {
            'Meta': {'object_name': 'RoomConfig'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.roompanorama': {
            'Meta': {'object_name': 'RoomPanorama'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'panorama': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'preview': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Room']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.roomphoto': {
            'Meta': {'object_name': 'RoomPhoto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.Room']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'room_config': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rooms.RoomConfig']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'rooms.season': {
            'Meta': {'object_name': 'Season'},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'special_offer_label': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['rooms']