# -*- coding: utf-8 -*-
from django import forms

from models import Order, RoomConfig, Season

class OrderForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        # self.fields['room_config'].initial = RoomConfig.objects.all()[0].id
        self.fields['season'].initial = Season.get_current().id

    class Meta:
        model = Order