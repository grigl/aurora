# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User

from pytils.translit import translify
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField

from apps.siteblocks.models import Settings
from managers import *
from mailers import send_order_email


def image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )

def hotel_image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), str(instance.id), translify(filename).replace(' ', '_') )

def panorama_file_path(instance, filename):
    return os.path.join('panoramas', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )


class RoomConfig(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'конфигурация комнат'
        verbose_name_plural = u'конфигурации комнат'


class HotelType(models.Model): 
    title = models.CharField(verbose_name=u'название', max_length=150)
    slug = models.SlugField(verbose_name=u'служебное название на латинице')
    label = models.CharField(verbose_name=u'подпись', max_length=150, blank=True)

    def __unicode__(self):
        return self.title + ' ' + self.label

    class Meta:
        verbose_name = u'тип отеля'
        verbose_name_plural = u'типы отелей'

    def get_rooms(self):
        hotel_list = self.hotel_set.all()
        room_list = []

        for hotel in hotel_list:
            for room in hotel.room_set.all():
                room_list.append(room)

        return room_list

    def get_all_photos(self):
        rooms = self.get_rooms()
        all_photos = []

        for room in rooms:
            for photo in room.roomphoto_set.all():
                all_photos.append(photo)

        return all_photos

    def has_photos(self):
        photos = self.get_all_photos()

        if len(photos) > 0:
            has_photos = True
        else: 
            has_photos = False

        return has_photos

    def get_all_panoramas(self):
        rooms = self.get_rooms()

        panoramas = []
        for room in rooms:
            for panorama in room.roompanorama_set.all():
                panoramas.append(panorama)

        return panoramas

    def has_panoramas(self):
        panoramas = self.get_all_panoramas()        

        if len(panoramas) > 0:
            has_panoramas = True
        else:
            has_panoramas = False

        return has_panoramas


class Hotel(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    slug = models.SlugField(verbose_name=u'служебное название на латинице')
    hotel_type = models.ForeignKey(HotelType, verbose_name=u'тип отеля')
    room_config = models.ForeignKey(RoomConfig, verbose_name=u'конфигурация комнат')
    short_description = models.TextField(verbose_name=u'краткое описание',blank=True)
    description = models.TextField(verbose_name=u'описание', blank=True)
    image = ImageField(verbose_name=u'картинка зима', upload_to=hotel_image_file_path)
    image_summer = ImageField(verbose_name=u'картинка лето', upload_to=hotel_image_file_path)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'отель'
        verbose_name_plural = u'отели'
        ordering = ['title']

    objects = models.Manager()
    townhouses = TownhousesManager()
    hotels = HotelsManager()

    def get_image(self):
        summer_or_winter = Settings.objects.get(name='summer_or_winter').value

        if summer_or_winter == 'winter':
            image = self.image
        else: 
            image = self.image_summer
             
        return image

    def get_rooms(self):
        room_list = self.room_set.all()

        return room_list

    def get_all_photos(self):
        rooms = self.get_rooms()
        all_photos = []

        for room in rooms:
            for photo in room.roomphoto_set.all():
                all_photos.append(photo)

        return all_photos

    def get_all_panoramas(self):
        rooms = self.room_set.all()

        panoramas = []
        for room in rooms:
            for panorama in room.roompanorama_set.all():
                panoramas.append(panorama)

        return panoramas

    def has_photos(self):
        photos = self.get_all_photos()

        if len(photos) > 0:
            has_photos = True
        else: 
            has_photos = False

        return has_photos

    def has_panoramas(self):
        panoramas = self.get_all_panoramas()        

        if len(panoramas) > 0:
            has_panoramas = True
        else:
            has_panoramas = False

        return has_panoramas


class RoomType(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    room_config = models.ForeignKey(RoomConfig,verbose_name=u'конфигурация комнат')

    def __unicode__(self):
        return self.title + u' из конфигурации ' + self.room_config.title

    class Meta:
        verbose_name = u'тип комнаты'
        verbose_name_plural = u'типы комнат'


class Room(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    hotel = models.ForeignKey(Hotel, verbose_name=u'отель')
    description = models.TextField(verbose_name=u'описание', blank=True)
    preview = models.FileField(verbose_name=u'превью сверху', upload_to=image_file_path)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'комната'
        verbose_name_plural = u'комнаты'

    def has_panoramas(self):
        if self.roompanorama_set.all().count() > 0:
            has_panoramas = True
        else:
            has_panoramas = False

        return has_panoramas


class RoomPhoto(models.Model):
    room = models.ForeignKey(Room, verbose_name=u'комната')
    title = models.CharField(verbose_name=u'название', max_length=150, blank=True)
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'фотография'
        verbose_name_plural = u'фотографии'


class RoomPanorama(models.Model):
    room = models.ForeignKey(Room, verbose_name=u'комната')
    title = models.CharField(verbose_name=u'название', max_length=150, blank=True)
    panorama = models.FileField(verbose_name=u'флэш файл панорамы', upload_to=panorama_file_path)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'панорама'
        verbose_name_plural = u'панорамы'


class PriceType(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    description = models.CharField(verbose_name=u'описание', max_length=150, blank=True)

    def __unicode__(self):
        return self.title + u' (%s)' % self.description

    class Meta:
        verbose_name = u'тип цены'
        verbose_name_plural = u'типы цен'


class Season(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    date_from = models.DateField(verbose_name=u'дата начала')
    date_to = models.DateField(verbose_name=u'дата конца')
    special_offer_label = models.TextField(verbose_name=u'подсказка о специальном предложении', blank=True)

    objects = models.Manager()
    not_archieved_objects = SeasonsNotArchievedManager()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'сезон'
        verbose_name_plural = u'сезоны'

    def get_price_types(self):
        price_list = self.price_set.all()
        price_list_type_ids = [x.price_type.id for x in price_list]
        price_types = PriceType.objects.filter(id__in=price_list_type_ids)

        return price_types

    def get_room_price_list(self): 
        room_price_list = RoomType.objects.order_by('room_config')

        for room_type in room_price_list:
            season_prices = [price for price in room_type.price_set.all() if price.season==self]
            room_type.season_prices = season_prices

        return room_price_list

    def has_prices(self):
        if self.price_set.all().count() > 0:
            has_prices = True
        else: 
            has_prices = False

        return has_prices

    def get_hotel_price_list(self, hotel):
        room_types_ids = [room_type.id for room_type in hotel.room_config.roomtype_set.all()]
        room_price_list = RoomType.objects.order_by('room_config').filter(id__in=room_types_ids)

        for room_type in room_price_list:
            room_type.season_prices = [price for price in room_type.price_set.all() if price.season==self]

        return room_price_list

    def has_hotel_prices(self, hotel):
        room_price_list = self.get_hotel_price_list(hotel)

        has_prices = False
        for room_type in room_price_list:
            if len(room_type.season_prices) > 0:
                has_prices = True

        return has_prices

    def get_townhouse_price_list(self, hotel_type):
        room_types_ids = [room_type.id for room_type in hotel_type.hotel_set.all()[0].room_config.roomtype_set.all()]
        room_price_list = RoomType.objects.order_by('room_config').filter(id__in=room_types_ids)

        for room_type in room_price_list:
            room_type.season_prices = [price for price in room_type.price_set.all() if price.season==self]

        return room_price_list

    def has_townhouse_prices(self, hotel_type):
        room_price_list = self.get_townhouse_price_list(hotel_type)

        has_prices = False
        for room_type in room_price_list:
            if len(room_type.season_prices) > 0:
                has_prices = True

        return has_prices

    def is_archieved(self):
        today = datetime.date.today()

        if self.date_to < today:
            is_archieved = True
        else:
            is_archieved = False

        return is_archieved

    def get_ordered_seasons(self):
        try:
            seasons = Season.objects.order_by('date_from', 'date_to')
        except:
            seasons = None

        return seasons

    def get_next_season(self):
        seasons = self.get_ordered_seasons()
        this_season = self

        if this_season in seasons:
            for position, season in enumerate(seasons):
                if this_season == season:
                    this_position = position
        else: 
            this_position = None

        if this_position is not None:
            try:
                next_season = seasons[this_position+1]
            except (IndexError, AssertionError):
                next_season = None
        else:
            next_season = None

        return next_season

    def get_prev_season(self):
        seasons = self.get_ordered_seasons()
        this_season = self

        if this_season in seasons:
            for position, season in enumerate(seasons):
                if this_season == season:
                    this_position = position
        else: 
            this_position = None

        if this_position is not None:
            try:
                prev_season = seasons[this_position-1]
            except (IndexError, AssertionError):
                prev_season = None
        else:
            prev_season = None

        return prev_season

    def get_this_pos(self):
        seasons = self.get_ordered_seasons()
        this_season = self

        if this_season in seasons:
            for position, season in enumerate(seasons):
                if this_season == season:
                    this_position = position
        else: 
            this_position = None

        return this_position

    def get_prev_not_archive_season(self):
        prev_season = self.get_prev_season()

        if prev_season and not prev_season.is_archieved():
            prev_season = prev_season
        else: 
            prev_season = None

        return prev_season


    @classmethod
    def get_current(cls):
        now = datetime.date.today()

        try:
            current_season = cls.objects.filter(date_from__lte=now).filter(date_to__gte=now)[0]
        except:
            current_season = cls.objects.order_by('-date_to')[0]

        return current_season

        
class Price(models.Model):
    room_type = models.ForeignKey(RoomType, verbose_name=u'тип комнаты')
    price_type = models.ForeignKey(PriceType, verbose_name=u'тип цены')
    season = models.ForeignKey(Season, verbose_name=u'сезон')
    price = models.CharField(verbose_name=u'цена', max_length=250, blank=True)

    def __unicode__(self):
        return str(self.price)

    class Meta:
        verbose_name = u'цена'
        verbose_name_plural = u'цены'
        ordering = ['price_type']

    def is_numeric(self):
        try:
            float(self.price)
            is_numeric = True
        except ValueError:
            is_numeric = False

        return is_numeric

    
class Restaurant(models.Model):
    restraunt_types = (('cafe','Кафе'),('restaurant','Ресторан'),('bar','Бар'))

    #  fields
    restaurant_type = models.CharField(verbose_name=u'тип ресторана', max_length=150, choices=restraunt_types) 
    title = models.CharField(verbose_name=u'название', max_length=150)
    description = models.TextField(verbose_name=u'описание')

    class Meta:
        verbose_name = u'ресторан'
        verbose_name_plural = u'рестораны'

    def __unicode__(self):
        return self.restaurant_type + u' ' + self.title
    

class RestaurantImage(models.Model):
    restaurant = models.ForeignKey(Restaurant, verbose_name=u'ресторан')
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)

    class Meta:
        verbose_name = u'фотография ресторана'
        verbose_name_plural = u'фотографии ресторана'

    def __unicode__(self):
        return self.image.name
    

class Order(models.Model):
    name = models.CharField(verbose_name=u'имя', max_length=150)
    phone = models.CharField(verbose_name=u'телефон', max_length=150)
    season = models.ForeignKey(Season, verbose_name=u'сезон')
    room_config = models.ForeignKey(RoomConfig, verbose_name=u'конфигурация комнат')
    desired_date = models.CharField(verbose_name=u'желаемая дата заезда', max_length=150)
    created_at = models.DateTimeField(verbose_name=u'время создания', editable=False, default=datetime.datetime.now)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'
        ordering = ['-created_at']

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        if self.pk is None:
            send_order_email(self)

        super(Order, self).save()
    
