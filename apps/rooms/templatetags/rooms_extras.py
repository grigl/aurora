# -*- coding: utf-8 -*-
import os

from apps.siteblocks.models import SiteMenu, Settings
from django import template

register = template.Library()


@register.inclusion_tag("rooms/get_room_config_hotel_images.html")
def get_room_config_hotel_images(room_config):
    hotel_set = room_config.hotel_set.all()

    hotels = []
    image_filenames = []

    for hotel in hotel_set:
        if not os.path.basename(hotel.image.name) in image_filenames:
            hotels.append(hotel)
            image_filenames.append(os.path.basename(hotel.image.name))

    return { 'hotels': hotels }


@register.inclusion_tag("rooms/get_order_form_hotel_images.html")
def get_order_form_hotel_images(room_config):
    hotel_set = room_config.hotel_set.all()

    hotels = []
    image_filenames = []

    for hotel in hotel_set:
        if not os.path.basename(hotel.image.name) in image_filenames:
            hotels.append(hotel)
            image_filenames.append(os.path.basename(hotel.image.name))

    return { 'hotels': hotels }


@register.inclusion_tag("rooms/get_order_room_config_title.html")
def get_order_room_config_title(room_config):
    hotel_list = room_config.hotel_set.all()

    return {'hotel_list':hotel_list, 'room_config':room_config}


@register.inclusion_tag("rooms/get_room_config_title.html")
def get_room_config_title(room_config):
    hotel_list = room_config.hotel_set.all()

    return {'hotel_list':hotel_list, 'room_config':room_config}

