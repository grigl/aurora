# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, FormView

from apps.siteblocks.models import TextBlock
from models import *
from forms import *


def get_season(request):
    if request.GET.get('season'):
        try:
            season = Season.objects.get(id=request.GET.get('season'))
        except:
            season = Season.get_current()
    else:
        season = Season.get_current()

    return season


class RoomsPriceListView(TemplateView):
    template_name = 'rooms/price_list.html'

    def get_context_data(self, **kwargs):
        context = super(RoomsPriceListView, self).get_context_data()

        season = get_season(self.request)

        room_price_list = season.get_room_price_list()

        context['room_price_list'] = room_price_list
        context['season_has_prices'] = season.has_prices
        context['season'] = season
        return context


class SeasonTableView(TemplateView):
    template_name = 'rooms/season_table.html'

    def get(self, request, **kwargs):
        context = self.get_context_data()
        html_code = render_to_string(self.template_name, context)
        return HttpResponse(html_code)

    def get_context_data(self, **kwargs):
        context = super(SeasonTableView, self).get_context_data()

        hotel_id = self.request.GET.get('hotel')

        if hotel_id:
            hotel = Hotel.objects.get(pk=hotel_id)
        else:
            hotel = None

        season = get_season(self.request)

        if hotel:
            room_price_list = season.get_hotel_price_list(hotel=hotel)
            season_has_prices = season.has_hotel_prices(hotel=hotel)
        else:
            room_price_list = season.get_room_price_list()
            season_has_prices = season.has_prices()

        context['room_price_list'] = room_price_list
        context['season_has_prices'] = season_has_prices
        context['season'] = season
        context['hotel'] = hotel_id
        return context


class HotelListView(TemplateView):
    template_name = 'rooms/hotel_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(HotelListView, self).get_context_data()

        hotel_list = Hotel.hotels.all()
        townhouse_list = Hotel.townhouses.all()

        context['hotel_list'] = hotel_list
        context['townhouse_list'] = townhouse_list
        return context


class HotelDetailView(TemplateView):
    template_name = 'rooms/hotel_detail.html'

    def get_hotel(self):
        hotel = Hotel.objects.get(slug=self.kwargs.get('slug'))

        return hotel

    def get_context_data(self, **kwargs):
        context = super(HotelDetailView, self).get_context_data()

        hotel = self.get_hotel()
        season = get_season(self.request)

        room_price_list = season.get_hotel_price_list(hotel=hotel)

        context['room_price_list'] = room_price_list
        context['season_has_prices'] = season.has_hotel_prices(hotel)
        context['season'] = season
        context['hotel'] = hotel
        return context


class TownhouseDetailView(TemplateView):
    template_name = 'rooms/hotel_detail.html'

    def get_hotel_type(self):
        hotel_type = HotelType.objects.get(slug=self.kwargs.get('slug'))

        return hotel_type

    def get_context_data(self, **kwargs):
        context = super(TownhouseDetailView, self).get_context_data()

        townhouse = self.get_hotel_type()

        if townhouse.slug == 'townhouse_6':
            townhouse_label = u' на 6 человек'
        elif townhouse.slug == 'townhouse_4':
            townhouse_label = u'на 4 человек'
        else:
            townhouse_label = u''

        season = get_season(self.request)

        room_price_list = season.get_townhouse_price_list(townhouse)

        context['room_price_list'] = room_price_list
        context['season_has_prices'] = season.has_townhouse_prices(townhouse)
        context['season'] = season
        context['hotel'] = townhouse
        context['townhouse_label'] = townhouse_label
        return context


class RestaurantsView(TemplateView):
    template_name = 'rooms/restaurants.html'

    def get_all_restaurants_images(self):
        restaurants_set = Restaurant.objects.all()
        images = []

        for restaurant in restaurants_set:
            for image in restaurant.restaurantimage_set.all():
                images.append(image)

        if len(images) < 1:
            images = None

        return images

    def get_context_data(self, **kwargs):
        context = super(RestaurantsView, self).get_context_data()

        restaurants = Restaurant.objects.all()
        all_restaurants_images = self.get_all_restaurants_images()

        context['restraunts_heading_text'] = TextBlock.objects.get(name='restraunts_heading_text').value
        context['restraunts_adults_text'] = TextBlock.objects.get(name='restraunts_adults_text').value
        context['restraunts_children_text'] = TextBlock.objects.get(name='restraunts_children_text').value
        context['restraunts_order_text'] = TextBlock.objects.get(name='restraunts_order_text').value
        context['restaurants'] = restaurants
        context['all_restaurants_images'] = all_restaurants_images
        return context


class OrderView(FormView):
    form_class = OrderForm
    template_name = 'rooms/order.html'
    success_url = '/thanks_order/'

    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data()

        context['form'] = self.get_form(self.form_class)
        context['room_config_list'] = RoomConfig.objects.all()
        context['season_list'] = Season.not_archieved_objects.all()
        return context

    def form_valid(self, form):
        Order.objects.create(**form.cleaned_data)
        return HttpResponseRedirect(self.get_success_url())
