# -*- coding: utf-8 -*-
import datetime 
from django.db import models
from django.db.models.query import QuerySet


class TownhousesManager(models.Manager):
    def get_query_set(self):
        return super(TownhousesManager, self).get_query_set().filter(hotel_type__slug__icontains='townhouse')


class HotelsManager(models.Manager):
    def get_query_set(self):
        return super(HotelsManager, self).get_query_set().filter(hotel_type__slug='hotel').order_by('hotel_type')


class SeasonsNotArchievedManager(models.Manager):
    def get_query_set(self):
        return super(SeasonsNotArchievedManager, self).get_query_set().filter(date_to__gt=datetime.date.today())