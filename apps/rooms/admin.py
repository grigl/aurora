# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *


class HotelTypeAdmin(admin.ModelAdmin):
    list_display = ('title','label')

admin.site.register(HotelType, HotelTypeAdmin)


class HotelAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor(attrs={'cols': 110, 'rows': 20}), required=False)
    description.label = u'Описание'

    class Meta:
        model = Hotel

class RoomInline(admin.TabularInline):
    model = Room
    extra = 0

class HotelAdmin(admin.ModelAdmin):
    list_display = ('title','hotel_type')
    inlines = [RoomInline]
    form = HotelAdminForm

admin.site.register(Hotel, HotelAdmin)


class RoomTypeInline(admin.TabularInline):
    model = RoomType
    extra = 0

class RoomConfigAdmin(admin.ModelAdmin):
    list_display = ('title',)
    inlines = [RoomTypeInline]

admin.site.register(RoomConfig, RoomConfigAdmin)


class PriceInline(admin.TabularInline):
    model = Price
    extra = 0

class RoomTypeAdmin(admin.ModelAdmin):
    list_display = ('title','get_room_config')
    inlines = [PriceInline]

    def get_room_config(self, obj):
        return obj.room_config.title
    get_room_config.short_description = u'конфигурация комнат'

admin.site.register(RoomType, RoomTypeAdmin)


class RoomPhotosInline(admin.TabularInline):
    model = RoomPhoto
    extra = 0

class RoomPanoramasInline(admin.TabularInline):
    model = RoomPanorama
    extra = 0

class RoomAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor(attrs={'cols': 110, 'rows': 20}), required=False)
    description.label = u'Описание'

    class Meta:
        model = Room

class RoomAdmin(admin.ModelAdmin):
    list_display = ('title','hotel')
    inlines = [RoomPhotosInline, RoomPanoramasInline]
    form = RoomAdminForm

admin.site.register(Room, RoomAdmin)


class SeasonAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Season, SeasonAdmin)


class PriceTypeAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(PriceType, PriceTypeAdmin)


class RestaurantAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor(attrs={'cols': 110, 'rows': 20}), required=False)
    description.label = u'Описание'

    class Meta:
        model = Restaurant

class RestaurantImageInline(admin.TabularInline):
    model = RestaurantImage
    extra = 0

class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('title','restaurant_type')
    inlines = [RestaurantImageInline]
    form = RestaurantAdminForm

admin.site.register(Restaurant, RestaurantAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('created_at','name', 'room_config', 'desired_date')

admin.site.register(Order, OrderAdmin)