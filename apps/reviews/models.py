# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User

from pytils.translit import translify
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField

from apps.siteblocks.models import Settings

from mailers import *


class Review(models.Model):
    initials = models.CharField(verbose_name=u'Инициалы', max_length=150)
    review = models.TextField(verbose_name=u'Отзыв')
    phone = models.CharField(verbose_name=u'Телефон', max_length=150)
    created_at = models.DateTimeField(verbose_name=u'Дата создания', default=datetime.datetime.now())
    is_published = models.BooleanField(verbose_name=u'опубликован', default=False)

    class Meta:
        verbose_name = u'отзыв'
        verbose_name_plural = u'отзывы'
        ordering = ['-created_at',]

    def __unicode__(self):
        return u'' + self.initials

    def save(self, **kwargs):
        if self.pk is None:
            send_review_email(self)

        super(Review, self).save()
