# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, FormView

from models import *
from forms import *

class ReviewListView(ListView):
    template_name = '/reviews/review_list.html'
    model = Review
    paginate_by = 7

    def get_queryset(self):
        return Review.objects.filter(is_published=True)


class ReviewFormView(FormView):
    template_name = 'reviews/review_form.html'
    form_class = ReviewForm
    success_template = 'reviews/review_success.html'

    def get_context_data(self, **kwargs):
        context = super(ReviewFormView, self).get_context_data()

        context['form'] = self.get_form(self.form_class)
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data()
        html_code = render_to_string(self.template_name, context, context_instance=RequestContext(request))
        return HttpResponse(html_code)

    def form_valid(self, form):
        Review.objects.create(**form.cleaned_data)
        return direct_to_template(self.request, self.success_template)



