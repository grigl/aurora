# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Review.phone'
        db.add_column('reviews_review', 'phone',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=150),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Review.phone'
        db.delete_column('reviews_review', 'phone')


    models = {
        'reviews.review': {
            'Meta': {'object_name': 'Review'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 7, 3, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'review': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['reviews']