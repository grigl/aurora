# -*- coding: utf-8 -*-
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from apps.siteblocks .models import Settings
from settings import DEFAULT_FROM_EMAIL as from_email

def send_review_email(order_instance):
    template_name = 'reviews/new_review_email.html'

    subject = 'новый отзыв'
    message = render_to_string(template_name, {'object':order_instance})
    to_email = Settings.objects.get(name='main_email').value

    msg = EmailMessage(subject, message, from_email, [to_email])
    msg.content_subtype = "html"
    msg.send()