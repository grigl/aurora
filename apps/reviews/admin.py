# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('initials','created_at')
    list_filter = ('is_published',)

admin.site.register(Review, ReviewAdmin)