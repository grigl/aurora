# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView

from models import *


class PressListView(ListView):
    template_name = 'press/press_list.html'
    context_object_name = 'press_list'
    paginate_by = 7

    def get_queryset(self):
        press_type = self.request.GET.get('press_type')

        if press_type:
            try:
                press_list = PressItem.objects.published().filter(press_type_slug=press_type)
            except:
                press_list = PressItem.objects.published()
        else:
            press_list = PressItem.objects.published()

        return press_list


class PressItemView(DetailView):
    model = PressItem