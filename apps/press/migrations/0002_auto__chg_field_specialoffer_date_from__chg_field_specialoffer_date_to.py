# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SpecialOffer.date_from'
        db.alter_column('press_specialoffer', 'date_from', self.gf('django.db.models.fields.DateField')(null=True))

        # Changing field 'SpecialOffer.date_to'
        db.alter_column('press_specialoffer', 'date_to', self.gf('django.db.models.fields.DateField')(null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SpecialOffer.date_from'
        raise RuntimeError("Cannot reverse this migration. 'SpecialOffer.date_from' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'SpecialOffer.date_to'
        raise RuntimeError("Cannot reverse this migration. 'SpecialOffer.date_to' and its values cannot be restored.")

    models = {
        'press.photoreport': {
            'Meta': {'object_name': 'PhotoReport', '_ormbases': ['press.PressItem']},
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'press.pressitem': {
            'Meta': {'object_name': 'PressItem'},
            'date_add': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 6, 5, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'press.pressphoto': {
            'Meta': {'object_name': 'PressPhoto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'photo_report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['press.PhotoReport']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'press.specialoffer': {
            'Meta': {'object_name': 'SpecialOffer', '_ormbases': ['press.PressItem']},
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['press']