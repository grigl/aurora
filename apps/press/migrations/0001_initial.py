# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PressItem'
        db.create_table('press_pressitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('short_text', self.gf('django.db.models.fields.TextField')()),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('date_add', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 6, 5, 0, 0))),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('press', ['PressItem'])

        # Adding model 'SpecialOffer'
        db.create_table('press_specialoffer', (
            ('pressitem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['press.PressItem'], unique=True, primary_key=True)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('date_from', self.gf('django.db.models.fields.DateField')()),
            ('date_to', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('press', ['SpecialOffer'])

        # Adding model 'PhotoReport'
        db.create_table('press_photoreport', (
            ('pressitem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['press.PressItem'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('press', ['PhotoReport'])

        # Adding model 'PressPhoto'
        db.create_table('press_pressphoto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('photo_report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['press.PhotoReport'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('press', ['PressPhoto'])


    def backwards(self, orm):
        # Deleting model 'PressItem'
        db.delete_table('press_pressitem')

        # Deleting model 'SpecialOffer'
        db.delete_table('press_specialoffer')

        # Deleting model 'PhotoReport'
        db.delete_table('press_photoreport')

        # Deleting model 'PressPhoto'
        db.delete_table('press_pressphoto')


    models = {
        'press.photoreport': {
            'Meta': {'object_name': 'PhotoReport', '_ormbases': ['press.PressItem']},
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'press.pressitem': {
            'Meta': {'object_name': 'PressItem'},
            'date_add': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 6, 5, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'press.pressphoto': {
            'Meta': {'object_name': 'PressPhoto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'photo_report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['press.PhotoReport']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'press.specialoffer': {
            'Meta': {'object_name': 'SpecialOffer', '_ormbases': ['press.PressItem']},
            'date_from': ('django.db.models.fields.DateField', [], {}),
            'date_to': ('django.db.models.fields.DateField', [], {}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['press']