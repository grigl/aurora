# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PressItem.press_type'
        db.add_column('press_pressitem', 'press_type',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=150),
                      keep_default=False)

        # Adding field 'PressItem.press_type_slug'
        db.add_column('press_pressitem', 'press_type_slug',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=150),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PressItem.press_type'
        db.delete_column('press_pressitem', 'press_type')

        # Deleting field 'PressItem.press_type_slug'
        db.delete_column('press_pressitem', 'press_type_slug')


    models = {
        'press.newsitem': {
            'Meta': {'object_name': 'NewsItem', '_ormbases': ['press.PressItem']},
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'press.photoreport': {
            'Meta': {'object_name': 'PhotoReport', '_ormbases': ['press.PressItem']},
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'press.pressitem': {
            'Meta': {'object_name': 'PressItem'},
            'date_add': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 6, 5, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'press_type': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'press_type_slug': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'short_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'press.pressphoto': {
            'Meta': {'object_name': 'PressPhoto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'photo_report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['press.PhotoReport']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'})
        },
        'press.specialoffer': {
            'Meta': {'object_name': 'SpecialOffer', '_ormbases': ['press.PressItem']},
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'pressitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['press.PressItem']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['press']