# -*- coding: utf-8 -*-
import os, datetime

from pytils.translit import translify

from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField

from apps.utils.managers import PublishedManager

def image_file_path(instance, filename):
    return os.path.join('images', 'press', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )

class PressItem(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    short_text = models.TextField(verbose_name=u'короткий текст', blank=True)    
    text = models.TextField(verbose_name=u'текст')
    date_add = models.DateField(verbose_name=u'дата добавления', default=datetime.date.today())
    is_published = models.BooleanField(verbose_name=u'опубликовано', default=True)
    press_type = models.CharField(verbose_name=u'тип', max_length=150, editable=False)
    press_type_slug = models.CharField(verbose_name=u'слаг типа', max_length=150, editable=False)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'пресс релиз'
        verbose_name_plural = u'пресс релизы'
        ordering = ['-date_add']

    def __unicode__(self):
        return self.title


class NewsItem(PressItem):
    objects = PublishedManager()

    class Meta:
        verbose_name = u'новость'
        verbose_name_plural = u'новости'

    def __unicode__(self):
        return self.title

    def save(self):
        if self.pk is None:
            self.press_type = 'новость'
            self.press_type_slug = 'news'
        super(NewsItem, self).save()

    
class SpecialOffer(PressItem):
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)
    date_from = models.DateField(verbose_name=u'дата начала', blank=True, null=True)
    date_to = models.DateField(verbose_name=u'дата окончания', blank=True, null=True)
    show_in_slider = models.BooleanField(verbose_name=u'показывать в слайдере', default=True)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'специальное предложение'
        verbose_name_plural = u'специальные предложения'

    def __unicode__(self):
        return self.title

    def save(self):
        self.press_type = 'специальное предложение'
        self.press_type_slug = 'special_offer'
        super(SpecialOffer, self).save()


class PhotoReport(PressItem):
    objects = PublishedManager()

    class Meta:
        verbose_name = u'фотоотчет'
        verbose_name_plural = u'фотоотчеты'

    def __unicode__(self):
        return self.title

    def save(self):
        self.press_type = 'фотоотчет'
        self.press_type_slug = 'photoreport'
        super(PhotoReport, self).save()


class PressPhoto(models.Model):
    photo_report = models.ForeignKey(PhotoReport, verbose_name=u'фототчет')
    title = models.CharField(verbose_name=u'название', max_length=150, blank=True)
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)

    class Meta:
        verbose_name = u'фотография'
        verbose_name_plural = u'фотографии'

    def __unicode__(self):
        return self.title
