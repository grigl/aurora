# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor
from models import *


class PressItemAdminForm(forms.ModelForm):
    text = forms.CharField(widget=Redactor, label=u'текст')

    class Meta:
        model = PressItem


class SpecialOfferAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_add', 'date_from', 'date_to', 'is_published')
    form = PressItemAdminForm

admin.site.register(SpecialOffer, SpecialOfferAdmin)


class PressPhotoInline(admin.TabularInline):
    model = PressPhoto
    extra = 0

class PhotoReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_add', 'is_published') 
    inlines = [PressPhotoInline]
    form = PressItemAdminForm

admin.site.register(PhotoReport, PhotoReportAdmin)


class NewsItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_add', 'is_published')
    form = PressItemAdminForm

admin.site.register(NewsItem, NewsItemAdmin)