# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'NewsCategory'
        db.create_table('newsboard_newscategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('newsboard', ['NewsCategory'])

        # Adding model 'News'
        db.create_table('newsboard_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['newsboard.NewsCategory'], null=True, blank=True)),
            ('image', self.gf('apps.newsboard.models.ImageField')(max_length=100, blank=True)),
            ('short_text', self.gf('django.db.models.fields.TextField')()),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('on_main_page', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_add', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal('newsboard', ['News'])


    def backwards(self, orm):
        # Deleting model 'NewsCategory'
        db.delete_table('newsboard_newscategory')

        # Deleting model 'News'
        db.delete_table('newsboard_news')


    models = {
        'newsboard.news': {
            'Meta': {'ordering': "['-date_add', '-id']", 'object_name': 'News'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['newsboard.NewsCategory']", 'null': 'True', 'blank': 'True'}),
            'date_add': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('apps.newsboard.models.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'on_main_page': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'newsboard.newscategory': {
            'Meta': {'ordering': "['-order']", 'object_name': 'NewsCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['newsboard']