# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'News.on_main_page'
        db.delete_column('newsboard_news', 'on_main_page')

        # Adding field 'News.date_end'
        db.add_column('newsboard_news', 'date_end',
                      self.gf('django.db.models.fields.DateField')(blank=True),
                      keep_default=False)


        # Changing field 'News.date_add'
        db.alter_column('newsboard_news', 'date_add', self.gf('django.db.models.fields.DateField')())

    def backwards(self, orm):
        # Adding field 'News.on_main_page'
        db.add_column('newsboard_news', 'on_main_page',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Deleting field 'News.date_end'
        db.delete_column('newsboard_news', 'date_end')


        # Changing field 'News.date_add'
        db.alter_column('newsboard_news', 'date_add', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        'newsboard.news': {
            'Meta': {'ordering': "['-date_add', '-id']", 'object_name': 'News'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['newsboard.NewsCategory']", 'null': 'True', 'blank': 'True'}),
            'date_add': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'date_end': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('apps.newsboard.models.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'newsboard.newscategory': {
            'Meta': {'ordering': "['-order']", 'object_name': 'NewsCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['newsboard']