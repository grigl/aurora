# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from apps.siteblocks.models import Gallery, TextBlock, Settings
import random

class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data()
        summer_locations = [('quadro','horse_riding','stadio',),
                           ('minigolf','boats','paintball',),
                           ('diving','fishing','sauna',),]

        winter_locations = [('quadro','horse_riding','sauna',),
                            ('skees','animation','diving',),
                            ('skees','fishing','sauna'),]

        try:
            summer_or_winter = Settings.objects.get(name='summer_or_winter').value
        except:
            summer_or_winter = False

        if summer_or_winter == 'summer':
            map_locations = random.choice(summer_locations)
        elif summer_or_winter == 'winter':
            map_locations = random.choice(winter_locations)
        else:
            map_locations = False

        context['map_locations'] = map_locations
        return context


index = IndexView.as_view()


class AboutView(TemplateView):
    template_name = 'about.html'

    def get_gallery_set(self):  
        gallery_set = Gallery.objects.published().filter(gallery_type='about')

        if gallery_set.count() == 0:
            gallery_set = None 

        return gallery_set

    def get_all_images(self):
        gallery_set = self.get_gallery_set()

        images = []
        for gallery in gallery_set:
            for photo in gallery.galleryimage_set.all():
                images.append(photo)

        return images

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data()

        gallery_set = self.get_gallery_set()
        all_gallery_set_images = self.get_all_images()

        context['gallery_set'] = gallery_set
        context['all_gallery_set_images'] = all_gallery_set_images
        context['about_resort_address'] = TextBlock.objects.get(name='about_resort_address').value
        context['about_office_address'] = TextBlock.objects.get(name='about_office_address').value
        return context


class CorpoPlacesView(TemplateView):
    template_name = 'corpo_places.html'

    def get_gallery_set(self):  
        gallery_set = Gallery.objects.published().filter(gallery_type='corpo_places')

        if gallery_set.count() == 0:
            gallery_set = None 

        return gallery_set

    def get_all_images(self):
        gallery_set = self.get_gallery_set()

        images = []
        for gallery in gallery_set:
            for photo in gallery.galleryimage_set.all():
                images.append(photo)

        return images

    def get_context_data(self, **kwargs):
        context = super(CorpoPlacesView, self).get_context_data()

        gallery_set = self.get_gallery_set()
        all_gallery_set_images = self.get_all_images()

        context['gallery_set'] = gallery_set
        context['all_gallery_set_images'] = all_gallery_set_images
        context['corpo_places_text'] = TextBlock.objects.get(name='corpo_places_text').value
        return context


class  TeambuildingView(TemplateView):
    template_name = 'teambuilding.html'

    def get_gallery_set(self):  
        gallery_set = Gallery.objects.published().filter(gallery_type='teambuilding')

        if gallery_set.count() == 0:
            gallery_set = None 

        return gallery_set

    def get_all_images(self):
        gallery_set = self.get_gallery_set()

        if gallery_set:
            images = []
            for gallery in gallery_set:
                for photo in gallery.galleryimage_set.all():
                    images.append(photo)
        else:
            images = None

        return images

    def get_context_data(self, **kwargs):
        context = super(TeambuildingView, self).get_context_data()

        gallery_set = self.get_gallery_set()
        all_gallery_set_images = self.get_all_images()
        try:
            teambuilding_text = TextBlock.objects.get(name='teambuilding_text').value
        except:
            teambuilding_text = None

        context['gallery_set'] = gallery_set
        context['all_gallery_set_images'] = all_gallery_set_images
        context['teambuilding_text'] = teambuilding_text
        return context


class  WeddingsView(TemplateView):
    template_name = 'weddings.html'

    def get_gallery_set(self):  
        gallery_set = Gallery.objects.published().filter(gallery_type='weddings')

        if gallery_set.count() == 0:
            gallery_set = None 

        return gallery_set

    def get_all_images(self):
        gallery_set = self.get_gallery_set()

        if gallery_set:
            images = []
            for gallery in gallery_set:
                for photo in gallery.galleryimage_set.all():
                    images.append(photo)
        else:
            images = None

        return images

    def get_context_data(self, **kwargs):
        context = super(WeddingsView, self).get_context_data()

        gallery_set = self.get_gallery_set()
        all_gallery_set_images = self.get_all_images()
        try:
            teambuilding_text = TextBlock.objects.get(name='weddings_text').value
        except:
            teambuilding_text = None

        context['gallery_set'] = gallery_set
        context['all_gallery_set_images'] = all_gallery_set_images
        context['weddings_text'] = teambuilding_text
        return context


class FullMapView(TemplateView):
    template_name = 'full_map.html'

    def get_context_data(self, **kwargs):
        context = super(FullMapView, self).get_context_data()

        try:
            summer_or_winter = Settings.objects.get(name='summer_or_winter').value
        except:
            summer_or_winter = False

        if summer_or_winter == 'summer':
            full_map_left_text = TextBlock.objects.get(name='summer_full_map_left_text').value
            full_map_right_text = TextBlock.objects.get(name='summer_full_map_right_text').value
        elif summer_or_winter == 'winter':
            full_map_left_text = TextBlock.objects.get(name='winter_full_map_left_text').value
            full_map_right_text = TextBlock.objects.get(name='winter_full_map_right_text').value
        else:
            full_map_left_text = ''
            full_map_right_text = ''

        context['full_map_left_text'] = full_map_left_text
        context['full_map_right_text'] = full_map_right_text
        return context
