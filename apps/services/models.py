# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User

from pytils.translit import translify
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField


def image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )


class Service(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    slug = models.SlugField(verbose_name=u'служебное название на латинице')
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)
    short_description = models.TextField(verbose_name=u'краткое описание', blank=True)    
    price_page_description = models.TextField(verbose_name=u'описание для страницы цен', blank=True)
    description = models.TextField(verbose_name=u'описание', blank=True)
    order = models.IntegerField(verbose_name=u'порядок сортировки', default = 10,
                                help_text = u'чем больше число, тем выше располагается элемент')

    class Meta:
        verbose_name = u'услуга'
        verbose_name_plural = u'услуги'
        ordering = ['-order']

    def __unicode__(self):
        return self.title

    def has_prices_or_price_description(self):
        if self.serviceprice_set.all().count() > 0 or self.price_page_description:
            return True
        else:
            return False

    def has_prices(self):
        if self.serviceprice_set.all().count() > 0:
            return True
        else:
            return False


class ServicePrice(models.Model):
    service = models.ForeignKey(Service, verbose_name=u'услуга')
    title = models.CharField(verbose_name=u'название', max_length=150)
    price = models.DecimalField(verbose_name=u'цена', max_digits=10, decimal_places=2)
    time = models.CharField(verbose_name=u'за какое время цена', max_length=50)

    class Meta:
        verbose_name = u'цена услуги'
        verbose_name_plural = u'цены услуг'

    def __unicode__(self):
        return self.title
    
    