# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView

from models import *


class ServiceListView(TemplateView):
    template_name = 'services/service_list.html' 

    def get_context_data(self, **kwargs):
        context = super(ServiceListView, self).get_context_data()

        service_list = Service.objects.all()

        context['service_list'] = service_list
        return context


class ServiceDetailView(TemplateView):
    template_name = 'services/service_detail.html' 

    def get_service(self):
        hotel = Service.objects.get(slug=self.kwargs.get('slug'))

        return hotel

    def get_context_data(self, **kwargs):
        context = super(ServiceDetailView, self).get_context_data()

        service = self.get_service()

        context['service'] = service
        return context


class ServicesPriceListView(TemplateView):
    template_name = 'services/service_price_list.html' 

    def get_context_data(self, **kwargs):
        context = super(ServicesPriceListView, self).get_context_data()

        service_list = Service.objects.all()
        services = []

        for service in service_list:
            if service.has_prices():
                services.append(service)

        context['service_list'] = services
        return context