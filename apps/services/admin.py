# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *


class ServiceAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor, label=u"описание")

    class Meta:
        model = Service

class ServicePriceInline(admin.TabularInline):
    model = ServicePrice
    extra = 0

class ServiceAdmin(admin.ModelAdmin):
    form = ServiceAdminForm
    list_display = ('title','order',)
    list_editable = ('order',)
    inlines = [ServicePriceInline]

admin.site.register(Service, ServiceAdmin)